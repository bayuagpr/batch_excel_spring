package com.spkk.backend.service;

import com.spkk.backend.model.VSummaryGoView;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface IVSummaryGoViewService {
    List<VSummaryGoView> semuaSumGoAr();
}
