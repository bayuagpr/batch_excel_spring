package com.spkk.backend.service;

import com.spkk.backend.model.VSummaryOthersView;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface IVSummaryOthersViewService {
    List<VSummaryOthersView> tabelDasbor();
}
