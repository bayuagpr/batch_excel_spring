package com.spkk.backend.service;

import com.spkk.backend.model.GroupOwnerEntity;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.List;

public interface IGroupOwnerEntityService {
    List<GroupOwnerEntity> semuaGrOwner();

    GroupOwnerEntity ambilGrOwner(BigDecimal id);

    void hapusGrOwner(BigDecimal id);

    GroupOwnerEntity simpanGrOwner(GroupOwnerEntity go);
}
