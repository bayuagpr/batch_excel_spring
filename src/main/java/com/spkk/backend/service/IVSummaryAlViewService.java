package com.spkk.backend.service;

import com.spkk.backend.model.VSummaryAlView;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface IVSummaryAlViewService {
    List<VSummaryAlView> semuaSumAl();
}
