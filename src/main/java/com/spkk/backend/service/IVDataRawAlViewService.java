package com.spkk.backend.service;

import com.spkk.backend.model.VDataRawAlView;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

public interface IVDataRawAlViewService {
    Page<VDataRawAlView> semuaAl(Pageable pageable);

    VDataRawAlView pilihAl(String alNo);
}
