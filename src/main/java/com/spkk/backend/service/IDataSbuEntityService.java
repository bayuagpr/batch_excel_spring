package com.spkk.backend.service;

import com.spkk.backend.model.DataSbuEntity;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.List;

public interface IDataSbuEntityService {
    List<DataSbuEntity> semuaTarget();

    DataSbuEntity ambilTarget(BigDecimal id);

    void hapusTarget(BigDecimal id);

    DataSbuEntity simpanTarget(DataSbuEntity ds);
}
