package com.spkk.backend.service.impl;

import com.spkk.backend.model.VSummaryOthersView;
import com.spkk.backend.repository.VSummaryOthersViewRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
* Generated by Spring Data Generator on 03/09/2018
*/
@Service
public class VSummaryOthersViewService implements com.spkk.backend.service.IVSummaryOthersViewService {

	private VSummaryOthersViewRepository vSummaryOthersViewRepository;

	@Autowired
	public VSummaryOthersViewService(VSummaryOthersViewRepository vSummaryOthersViewRepository) {
		this.vSummaryOthersViewRepository = vSummaryOthersViewRepository;
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	@Override
	public List<VSummaryOthersView> tabelDasbor() {
		return vSummaryOthersViewRepository.findAll();
	}


}
