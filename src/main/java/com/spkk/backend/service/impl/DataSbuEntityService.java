package com.spkk.backend.service.impl;

import com.spkk.backend.model.DataSbuEntity;
import com.spkk.backend.repository.DataSbuEntityRepository;
import com.spkk.backend.service.IDataSbuEntityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.List;

/**
* Generated by Spring Data Generator on 03/09/2018
*/
@Service
public class DataSbuEntityService implements IDataSbuEntityService {

	private DataSbuEntityRepository dataSbuEntityRepository;

	@Autowired
	public DataSbuEntityService(DataSbuEntityRepository dataSbuEntityRepository) {
		this.dataSbuEntityRepository = dataSbuEntityRepository;
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	@Override
	public List<DataSbuEntity> semuaTarget() {
		return dataSbuEntityRepository.findAll();
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	@Override
	public DataSbuEntity ambilTarget(BigDecimal id) {
		return dataSbuEntityRepository.getOne(id);
	}

	@Transactional(propagation = Propagation.REQUIRED)
	@Override
	public void hapusTarget(BigDecimal id) {
		dataSbuEntityRepository.deleteById(id);
	}

	@Transactional(propagation = Propagation.REQUIRED)
	@Override
	public DataSbuEntity simpanTarget(DataSbuEntity ds) {
		return dataSbuEntityRepository.save(ds);
	}



}

