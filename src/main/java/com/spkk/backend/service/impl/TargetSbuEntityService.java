package com.spkk.backend.service.impl;

import com.spkk.backend.model.TargetSbuEntity;
import com.spkk.backend.repository.TargetSbuEntityRepository;
import com.spkk.backend.service.ITargetSbuEntityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.List;

/**
* Generated by Spring Data Generator on 03/09/2018
*/
@Service
public class TargetSbuEntityService implements ITargetSbuEntityService {

	private TargetSbuEntityRepository targetSbuEntityRepository;

	@Autowired
	public TargetSbuEntityService(TargetSbuEntityRepository targetSbuEntityRepository) {
		this.targetSbuEntityRepository = targetSbuEntityRepository;
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	@Override
	public List<TargetSbuEntity> semuaTarget() {
		return targetSbuEntityRepository.findAll();
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	@Override
	public TargetSbuEntity ambilTarget(BigDecimal id) {
		return targetSbuEntityRepository.getOne(id);
	}

	@Transactional(propagation = Propagation.REQUIRED)
	@Override
	public void hapusTarget(BigDecimal id) {
		targetSbuEntityRepository.deleteById(id);
	}

	@Transactional(propagation = Propagation.REQUIRED)
	@Override
	public TargetSbuEntity simpanTarget(TargetSbuEntity ds) {
		return targetSbuEntityRepository.save(ds);
	}

}
