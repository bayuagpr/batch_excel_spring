package com.spkk.backend.service.impl;

import com.spkk.backend.model.VAlEstDecFixView;
import com.spkk.backend.repository.VAlEstDecFixViewRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

/**
* Generated by Spring Data Generator on 03/09/2018
*/
@Service
public class VAlEstDecFixViewService {

	private VAlEstDecFixViewRepository vAlEstDecFixViewRepository;

	@Autowired
	public VAlEstDecFixViewService(VAlEstDecFixViewRepository vAlEstDecFixViewRepository) {
		this.vAlEstDecFixViewRepository = vAlEstDecFixViewRepository;
	}

}
