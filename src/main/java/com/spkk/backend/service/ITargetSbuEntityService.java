package com.spkk.backend.service;

import com.spkk.backend.model.TargetSbuEntity;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.List;

public interface ITargetSbuEntityService {
    List<TargetSbuEntity> semuaTarget();

    TargetSbuEntity ambilTarget(BigDecimal id);

    void hapusTarget(BigDecimal id);

    TargetSbuEntity simpanTarget(TargetSbuEntity ds);
}
