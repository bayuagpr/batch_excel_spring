package com.spkk.backend.service;

import com.spkk.backend.model.VDataRawArView;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

public interface IVDataRawArViewService {
    Page<VDataRawArView> semuaAr(Pageable pageable);

    VDataRawArView pilihAr(String arNo);
}
