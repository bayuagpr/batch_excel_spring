package com.spkk.backend.service;

import com.spkk.backend.model.GroupLayananEntity;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.List;

public interface IGroupLayananEntityService {
    List<GroupLayananEntity> semuaGrLayanan();

    GroupLayananEntity ambilGrLayanan(BigDecimal id);

    void hapusGrLayanan(BigDecimal id);

    GroupLayananEntity simpanGrLayanan(GroupLayananEntity ds);
}
