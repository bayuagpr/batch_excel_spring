package com.spkk.backend.service;

import com.spkk.backend.model.VSummaryGlView;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface IVSummaryGlViewService {
    List<VSummaryGlView> semuaSumGlAr();
}
