package com.spkk.backend.controller;

import com.spkk.backend.model.GroupLayananEntity;
import com.spkk.backend.service.IGroupLayananEntityService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.math.BigDecimal;
import java.util.List;

@Slf4j
@RestController
@CrossOrigin
@RequestMapping("/api/v1/grLayanan")
public class GroupLayananController {

    private IGroupLayananEntityService restService;

    @Autowired
    public GroupLayananController(IGroupLayananEntityService restService) {
        this.restService = restService;
    }

    @GetMapping("/tampilkan")
    public ResponseEntity<List<GroupLayananEntity>> findAll(){
        List< GroupLayananEntity > entities = restService.semuaGrLayanan();
        return ResponseEntity.ok().body(entities);
    }

    @PostMapping
    @ResponseBody
    public ResponseEntity<GroupLayananEntity> create(@RequestBody GroupLayananEntity entity ){

        restService.simpanGrLayanan(entity);
        return ResponseEntity.ok().body(entity);
    }

    @PutMapping("/renewal/{id}")
    public ResponseEntity<?> updateOne(@PathVariable("id") BigDecimal id, @Valid @RequestBody  GroupLayananEntity entity ){
        GroupLayananEntity r =restService.ambilGrLayanan(id);
        r.setGroupLayanan(entity.getGroupLayanan());
        r.setLayanan(entity.getLayanan());
        r.setJenis(entity.getJenis());
        restService.simpanGrLayanan(r);
        return ResponseEntity.ok().body("Data terbaru pada id "+ id);
    }

    @DeleteMapping("/hapus/{id}")
    public ResponseEntity<?> delete(@PathVariable("id") BigDecimal id){
        restService.hapusGrLayanan(id);
        return ResponseEntity.ok().body("Sukses terhapus.");
    }
}
