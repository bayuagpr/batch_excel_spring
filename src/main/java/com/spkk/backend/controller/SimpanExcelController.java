package com.spkk.backend.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.util.*;

@Slf4j
@RestController
@CrossOrigin
@RequestMapping("/api/v1/upload")
public class SimpanExcelController {

    private final Job jobAlUpload;
    private final Job jobArUpload;

    private final JobLauncher jobLauncher;

    @Autowired
    SimpanExcelController(@Qualifier("excelAlFileToDatabaseJob") Job jobAlUpload, @Qualifier("excelArFileToDatabaseJob") Job jobArUpload, JobLauncher jobLauncher) {
        this.jobAlUpload = jobAlUpload;
        this.jobArUpload = jobArUpload;
        this.jobLauncher = jobLauncher;
    }

    @PostMapping("/alUpload")
    @ResponseBody
    public ResponseEntity<?> alUpload(@RequestParam("file") MultipartFile file) throws Exception {
        final String label = "al" + ".xlsx";
        final String filepath = "/tmp/" + label;
        byte[] bytes = file.getBytes();
        File fh = new File("/tmp/");
        if(!fh.exists()){
            fh.mkdir();
        }
        try {
            FileOutputStream writer = new FileOutputStream(filepath);
            writer.write(bytes);
            writer.close();

            log.info("image bytes received: {}", bytes.length);


        }catch(IOException ex) {
            log.error("Failed to process the uploaded file", ex);
        }

        log.info("Starting excelAlFileToDatabase job");
        jobLauncher.run(jobAlUpload, new JobParametersBuilder().addDate("dateLaunch",new Date()).toJobParameters());
        log.info("Stopping excelAlFileToDatabase job");
        return ResponseEntity.ok().body("File uploaded");
    }



    @PostMapping("/arUpload")
    @ResponseBody
    public ResponseEntity<?> arUpload(@RequestParam("file") MultipartFile file) throws Exception {
        final String label = "ar" + ".xlsx";
        final String filepath = "/tmp/" + label;
        byte[] bytes = file.getBytes();
        File fh = new File("/tmp/");
        if(!fh.exists()){
            fh.mkdir();
        }
        try {
            FileOutputStream writer = new FileOutputStream(filepath);
            writer.write(bytes);
            writer.close();

            log.info("image bytes received: {}", bytes.length);

        }catch(IOException ex) {
            log.error("Failed to process the uploaded file", ex);
        }
        log.info("Starting excelArFileToDatabase job");
        jobLauncher.run(jobArUpload, new JobParametersBuilder().addDate("dateLaunch",new Date()).toJobParameters());
        log.info("Stopping excelArFileToDatabase job");
        return ResponseEntity.ok().body("File uploaded");
    }




}
