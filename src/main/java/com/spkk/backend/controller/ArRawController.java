package com.spkk.backend.controller;


import com.spkk.backend.model.VDataRawArView;
import com.spkk.backend.service.IVDataRawArViewService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@Slf4j
@RestController
@CrossOrigin
@RequestMapping("/api/v1/view/rawAr")
public class ArRawController {
    private IVDataRawArViewService restService;

    @Autowired
    public ArRawController(IVDataRawArViewService restService) {
        this.restService = restService;
    }

    @GetMapping("/ambil")
    @ResponseBody
    public ResponseEntity<VDataRawArView> findOne(@RequestParam("id") String id ){
        log.info("Id request: "+id);
        VDataRawArView entity = restService.pilihAr(id);
        log.info("Hasil pencarian: "+entity.toString());
        return ResponseEntity.ok().body(entity);


    }

    @GetMapping("/tampilkan")
    public ResponseEntity<Page< VDataRawArView >> findAll(Pageable paging){
        Page< VDataRawArView > entities = restService.semuaAr(paging);
        log.info("halaman: "+entities.getNumber()+"\n");
        log.info("Banyak row: "+entities.getTotalElements()+"\n");
        log.info("Banyak halaman: "+entities.getTotalPages()+"\n");
        return ResponseEntity.ok().body(entities);
    }
}
