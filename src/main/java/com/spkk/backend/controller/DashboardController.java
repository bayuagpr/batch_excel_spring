package com.spkk.backend.controller;

import com.spkk.backend.model.VSummaryOthersView;
import com.spkk.backend.service.IVSummaryOthersViewService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Slf4j
@RestController
@CrossOrigin
@RequestMapping("/api/v1/dashboard")
public class DashboardController {
    private IVSummaryOthersViewService restService;

    @Autowired
    public DashboardController(IVSummaryOthersViewService restService) {
        this.restService = restService;
    }

    @GetMapping("/tablelDashboard")
    public ResponseEntity<List<VSummaryOthersView>> findAll(){
        List< VSummaryOthersView > entities = restService.tabelDasbor();
        return ResponseEntity.ok().body(entities);
    }
}
