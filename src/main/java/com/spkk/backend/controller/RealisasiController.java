package com.spkk.backend.controller;


import com.spkk.backend.model.VSummaryAlView;
import com.spkk.backend.service.IVSummaryAlViewService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Slf4j
@RestController
@CrossOrigin
@RequestMapping("/api/v1/realisasi")
public class RealisasiController {
    private IVSummaryAlViewService restService;

    @Autowired
    public RealisasiController(IVSummaryAlViewService restService) {
        this.restService = restService;
    }

    @GetMapping("/tampilkan")
    public ResponseEntity<List<VSummaryAlView>> findAll(){
        List< VSummaryAlView > entities = restService.semuaSumAl();
        return ResponseEntity.ok().body(entities);
    }
}
