package com.spkk.backend.controller;

import com.spkk.backend.model.VDataRawAlView;
import com.spkk.backend.service.IVDataRawAlViewService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@Slf4j
@RestController
@CrossOrigin
@RequestMapping("/api/v1/view/rawAl")
public class AlRawController {

    private IVDataRawAlViewService restService;

    @Autowired
    public AlRawController(IVDataRawAlViewService restService) {
        this.restService = restService;
    }

    @GetMapping("/ambil")
    @ResponseBody
    public ResponseEntity<VDataRawAlView> findOne(@RequestParam("id") String id ){
        log.info("Id request: "+id);
        VDataRawAlView entity = restService.pilihAl(id);
        log.info("Hasil pencarian: "+entity.toString());
        return ResponseEntity.ok().body(entity);


    }

    @GetMapping("/tampilkan")
    public ResponseEntity<Page< VDataRawAlView >> findAll(Pageable paging){

            //Pageable paging = PageRequest.of(page, size);
            Page< VDataRawAlView > entities = restService.semuaAl(paging);
            log.info("halaman: "+entities.getNumber()+"\n");
            log.info("Banyak row: "+entities.getTotalElements()+"\n");
            log.info("Banyak halaman: "+entities.getTotalPages()+"\n");
            return ResponseEntity.ok().body(entities);


    }

//    @GetMapping("/tampilkan")
//    public ResponseEntity<Page< VDataRawAlView >> findAllFirst(@RequestParam("size") Integer size){
//        Pageable paging = PageRequest.of(0, size);
//        Page< VDataRawAlView > entities = restService.semuaAl(paging);
//        log.info("Banyak row: "+entities.getTotalElements()+"\n");
//        log.info("Banyak halaman: "+entities.getTotalPages()+"\n");
//        return ResponseEntity.ok().body(entities);
//    }
}
