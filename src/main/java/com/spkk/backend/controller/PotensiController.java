package com.spkk.backend.controller;


import com.spkk.backend.model.VSummaryGlView;
import com.spkk.backend.model.VSummaryGoView;
import com.spkk.backend.service.IVSummaryGlViewService;
import com.spkk.backend.service.IVSummaryGoViewService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Slf4j
@RestController
@CrossOrigin
@RequestMapping("/api/v1/potensi")
public class PotensiController {
    private IVSummaryGlViewService restServiceGl;
    private IVSummaryGoViewService restServiceGo;

    @Autowired
    public PotensiController(IVSummaryGlViewService restServiceGl, IVSummaryGoViewService restServiceGo) {
        this.restServiceGl = restServiceGl;
        this.restServiceGo = restServiceGo;
    }

    @GetMapping("/tampilkanGo")
    public ResponseEntity<List<VSummaryGoView>> findAllGo(){
        List< VSummaryGoView > entities = restServiceGo.semuaSumGoAr();
        return ResponseEntity.ok().body(entities);
    }

    @GetMapping("/tampilkanGl")
    public ResponseEntity<List<VSummaryGlView >> findAllGl(){
        List< VSummaryGlView > entities = restServiceGl.semuaSumGlAr();
        return ResponseEntity.ok().body(entities);
    }
}
