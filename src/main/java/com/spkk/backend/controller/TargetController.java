package com.spkk.backend.controller;

import com.spkk.backend.model.TargetSbuEntity;
import com.spkk.backend.service.ITargetSbuEntityService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.math.BigDecimal;
import java.util.List;

@Slf4j
@RestController
@CrossOrigin
@RequestMapping("/api/v1/target")
public class TargetController {

    private ITargetSbuEntityService restService;

    @Autowired
    public TargetController(ITargetSbuEntityService restService) {
        this.restService = restService;
    }

    @GetMapping("/tampilkan")
    public ResponseEntity<List< TargetSbuEntity >> findAll(){
        List< TargetSbuEntity > entities = restService.semuaTarget();
        return ResponseEntity.ok().body(entities);
    }

    @PostMapping
    @ResponseBody
    public ResponseEntity<TargetSbuEntity> create(@RequestBody TargetSbuEntity entity ){

        restService.simpanTarget(entity);
        return ResponseEntity.ok().body(entity);
    }

    @PutMapping("/renewal/{id}")
    public ResponseEntity<?> updateOne(@PathVariable("id") BigDecimal id, @Valid @RequestBody  TargetSbuEntity entity ){
        TargetSbuEntity r =restService.ambilTarget(id);
        r.setGroupOwner(entity.getGroupOwner());
        r.setTahun(entity.getTahun());
        r.setTargetSbu(entity.getTargetSbu());
        restService.simpanTarget(r);
        return ResponseEntity.ok().body("Target terbaru pada id "+ id);
    }

    @DeleteMapping("/hapus/{id}")
    public ResponseEntity<?> delete(@PathVariable("id") BigDecimal id){
        restService.hapusTarget(id);
        return ResponseEntity.ok().body("Sukses terhapus.");
    }
}
