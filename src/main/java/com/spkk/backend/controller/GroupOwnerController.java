package com.spkk.backend.controller;


import com.spkk.backend.model.GroupOwnerEntity;
import com.spkk.backend.service.IGroupOwnerEntityService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.math.BigDecimal;
import java.util.List;

@Slf4j
@RestController
@CrossOrigin
@RequestMapping("/api/v1/grOwner")
public class GroupOwnerController {
    private IGroupOwnerEntityService restService;

    @Autowired
    public GroupOwnerController(IGroupOwnerEntityService restService) {
        this.restService = restService;
    }

    @GetMapping("/tampilkan")
    public ResponseEntity<List<GroupOwnerEntity>> findAll(){
        List< GroupOwnerEntity > entities = restService.semuaGrOwner();
        return ResponseEntity.ok().body(entities);
    }

    @PostMapping
    @ResponseBody
    public ResponseEntity<GroupOwnerEntity> create(@RequestBody GroupOwnerEntity entity ){

        restService.simpanGrOwner(entity);
        return ResponseEntity.ok().body(entity);
    }

    @PutMapping("/renewal/{id}")
    public ResponseEntity<?> updateOne(@PathVariable("id") BigDecimal id, @Valid @RequestBody  GroupOwnerEntity entity ){
        GroupOwnerEntity r =restService.ambilGrOwner(id);
        r.setGroupOwner(entity.getGroupOwner());
        r.setSbuOwner(entity.getSbuOwner());
        r.setSegment(entity.getSegment());
        restService.simpanGrOwner(r);
        return ResponseEntity.ok().body("Data terbaru pada id "+ id);
    }

    @DeleteMapping("/hapus/{id}")
    public ResponseEntity<?> delete(@PathVariable("id") BigDecimal id){
        restService.hapusGrOwner(id);
        return ResponseEntity.ok().body("Sukses terhapus.");
    }
}
