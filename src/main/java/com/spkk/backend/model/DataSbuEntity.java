/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.spkk.backend.model;

import lombok.ToString;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author bayua
 */
@Entity
@Table(name = "DATA_SBU")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DataSbuEntity.findAll", query = "SELECT d FROM DataSbuEntity d")
    , @NamedQuery(name = "DataSbuEntity.findBySbu", query = "SELECT d FROM DataSbuEntity d WHERE d.sbu = :sbu")
    , @NamedQuery(name = "DataSbuEntity.findByTahun", query = "SELECT d FROM DataSbuEntity d WHERE d.tahun = :tahun")
    , @NamedQuery(name = "DataSbuEntity.findByTarget", query = "SELECT d FROM DataSbuEntity d WHERE d.target = :target")
    , @NamedQuery(name = "DataSbuEntity.findById", query = "SELECT d FROM DataSbuEntity d WHERE d.dataSbuId = :dataSbuId")})
@ToString(includeFieldNames=true)
public class DataSbuEntity implements Serializable {

    private static final long serialVersionUID = 1L;
    @Size(max = 100)
    @Column(name = "SBU")
    private String sbu;
    @Column(name = "TAHUN")
    private BigInteger tahun;
    @Column(name = "TARGET")
    private BigInteger target;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "DATA_SBU_ID")
    private BigDecimal dataSbuId;

    public DataSbuEntity() {
    }

    public DataSbuEntity(BigDecimal dataSbuId) {
        this.dataSbuId = dataSbuId;
    }

    public String getSbu() {
        return sbu;
    }

    public void setSbu(String sbu) {
        this.sbu = sbu;
    }

    public BigInteger getTahun() {
        return tahun;
    }

    public void setTahun(BigInteger tahun) {
        this.tahun = tahun;
    }

    public BigInteger getTarget() {
        return target;
    }

    public void setTarget(BigInteger target) {
        this.target = target;
    }

    public BigDecimal getDataSbuId() {
        return dataSbuId;
    }

    public void setDataSbuId(BigDecimal id) {
        this.dataSbuId = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (dataSbuId != null ? dataSbuId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DataSbuEntity)) {
            return false;
        }
        DataSbuEntity other = (DataSbuEntity) object;
        if ((this.dataSbuId == null && other.dataSbuId != null) || (this.dataSbuId != null && !this.dataSbuId.equals(other.dataSbuId))) {
            return false;
        }
        return true;
    }


}
