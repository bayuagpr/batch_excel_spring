/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.spkk.backend.model;

import lombok.ToString;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author bayua
 */
@Entity
@Table(name = "TARGET_SBU")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TargetSbuEntity.findAll", query = "SELECT t FROM TargetSbuEntity t")
    , @NamedQuery(name = "TargetSbuEntity.findByGroupOwner", query = "SELECT t FROM TargetSbuEntity t WHERE t.groupOwner = :groupOwner")
    , @NamedQuery(name = "TargetSbuEntity.findByTahun", query = "SELECT t FROM TargetSbuEntity t WHERE t.tahun = :tahun")
    , @NamedQuery(name = "TargetSbuEntity.findByTarget", query = "SELECT t FROM TargetSbuEntity t WHERE t.targetSbu = :target")
    , @NamedQuery(name = "TargetSbuEntity.findById", query = "SELECT t FROM TargetSbuEntity t WHERE t.targetSbuId = :id")})
@ToString(includeFieldNames=true)
public class TargetSbuEntity implements Serializable {

    private static final long serialVersionUID = 1L;
    @Size(max = 100)
    @Column(name = "GROUP_OWNER")
    private String groupOwner;
    @Column(name = "TAHUN")
    private BigInteger tahun;
    @Column(name = "TARGET")
    private BigInteger targetSbu;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "TARGET_SBU_ID")
    private BigDecimal targetSbuId;

    public TargetSbuEntity() {
    }

    public TargetSbuEntity(BigDecimal targetSbuId) {
        this.targetSbuId = targetSbuId;
    }

    public String getGroupOwner() {
        return groupOwner;
    }

    public void setGroupOwner(String groupOwner) {
        this.groupOwner = groupOwner;
    }

    public BigInteger getTahun() {
        return tahun;
    }

    public void setTahun(BigInteger tahun) {
        this.tahun = tahun;
    }

    public BigInteger getTargetSbu() {
        return targetSbu;
    }

    public void setTargetSbu(BigInteger target) {
        this.targetSbu = target;
    }

    public BigDecimal getTargetSbuId() {
        return targetSbuId;
    }

    public void setTargetSbuId(BigDecimal id) {
        this.targetSbuId = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (targetSbuId != null ? targetSbuId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TargetSbuEntity)) {
            return false;
        }
        TargetSbuEntity other = (TargetSbuEntity) object;
        if ((this.targetSbuId == null && other.targetSbuId != null) || (this.targetSbuId != null && !this.targetSbuId.equals(other.targetSbuId))) {
            return false;
        }
        return true;
    }


}
