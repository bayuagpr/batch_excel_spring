
package com.spkk.backend.model;

import lombok.ToString;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;


@Entity
@Table(name = "ACTIVATION_REQUEST", schema = "WAHYUTONI")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ActivationRequestEntity.findAll", query = "SELECT a FROM ActivationRequestEntity a")
    , @NamedQuery(name = "ActivationRequestEntity.findByArNo", query = "SELECT a FROM ActivationRequestEntity a WHERE a.arNo = :arNo")
    , @NamedQuery(name = "ActivationRequestEntity.findByArType", query = "SELECT a FROM ActivationRequestEntity a WHERE a.arType = :arType")
    , @NamedQuery(name = "ActivationRequestEntity.findByCustName", query = "SELECT a FROM ActivationRequestEntity a WHERE a.custName = :custName")
    , @NamedQuery(name = "ActivationRequestEntity.findBySegment", query = "SELECT a FROM ActivationRequestEntity a WHERE a.segment = :segment")
    , @NamedQuery(name = "ActivationRequestEntity.findByArStatus", query = "SELECT a FROM ActivationRequestEntity a WHERE a.arStatus = :arStatus")
    , @NamedQuery(name = "ActivationRequestEntity.findByArDate", query = "SELECT a FROM ActivationRequestEntity a WHERE a.arDate = :arDate")
    , @NamedQuery(name = "ActivationRequestEntity.findByBaDate", query = "SELECT a FROM ActivationRequestEntity a WHERE a.baDate = :baDate")
    , @NamedQuery(name = "ActivationRequestEntity.findByCancelDate", query = "SELECT a FROM ActivationRequestEntity a WHERE a.cancelDate = :cancelDate")
    , @NamedQuery(name = "ActivationRequestEntity.findByApprovalDate", query = "SELECT a FROM ActivationRequestEntity a WHERE a.approvalDate = :approvalDate")
    , @NamedQuery(name = "ActivationRequestEntity.findByTargetdate", query = "SELECT a FROM ActivationRequestEntity a WHERE a.targetdate = :targetdate")
    , @NamedQuery(name = "ActivationRequestEntity.findByClosedate", query = "SELECT a FROM ActivationRequestEntity a WHERE a.closedate = :closedate")
    , @NamedQuery(name = "ActivationRequestEntity.findByActivationactualdate", query = "SELECT a FROM ActivationRequestEntity a WHERE a.activationactualdate = :activationactualdate")
    , @NamedQuery(name = "ActivationRequestEntity.findByDeactivationdate", query = "SELECT a FROM ActivationRequestEntity a WHERE a.deactivationdate = :deactivationdate")
    , @NamedQuery(name = "ActivationRequestEntity.findBySid", query = "SELECT a FROM ActivationRequestEntity a WHERE a.sid = :sid")
    , @NamedQuery(name = "ActivationRequestEntity.findByLayanan", query = "SELECT a FROM ActivationRequestEntity a WHERE a.layanan = :layanan")
    , @NamedQuery(name = "ActivationRequestEntity.findByQty", query = "SELECT a FROM ActivationRequestEntity a WHERE a.qty = :qty")
    , @NamedQuery(name = "ActivationRequestEntity.findByBandwidth", query = "SELECT a FROM ActivationRequestEntity a WHERE a.bandwidth = :bandwidth")
    , @NamedQuery(name = "ActivationRequestEntity.findByBiayaSewa", query = "SELECT a FROM ActivationRequestEntity a WHERE a.biayaSewa = :biayaSewa")
    , @NamedQuery(name = "ActivationRequestEntity.findByBiayaInstalasi", query = "SELECT a FROM ActivationRequestEntity a WHERE a.biayaInstalasi = :biayaInstalasi")
    , @NamedQuery(name = "ActivationRequestEntity.findByBiayaRelokasi", query = "SELECT a FROM ActivationRequestEntity a WHERE a.biayaRelokasi = :biayaRelokasi")
    , @NamedQuery(name = "ActivationRequestEntity.findByAddressOriginating", query = "SELECT a FROM ActivationRequestEntity a WHERE a.addressOriginating = :addressOriginating")
    , @NamedQuery(name = "ActivationRequestEntity.findBySbuOri", query = "SELECT a FROM ActivationRequestEntity a WHERE a.sbuOri = :sbuOri")
    , @NamedQuery(name = "ActivationRequestEntity.findByAddressTerminating", query = "SELECT a FROM ActivationRequestEntity a WHERE a.addressTerminating = :addressTerminating")
    , @NamedQuery(name = "ActivationRequestEntity.findBySbuTer", query = "SELECT a FROM ActivationRequestEntity a WHERE a.sbuTer = :sbuTer")
    , @NamedQuery(name = "ActivationRequestEntity.findByOwner", query = "SELECT a FROM ActivationRequestEntity a WHERE a.owner = :owner")
    , @NamedQuery(name = "ActivationRequestEntity.findBySbuOwner", query = "SELECT a FROM ActivationRequestEntity a WHERE a.sbuOwner = :sbuOwner")
    , @NamedQuery(name = "ActivationRequestEntity.findByAgrId", query = "SELECT a FROM ActivationRequestEntity a WHERE a.agrId = :agrId")
    , @NamedQuery(name = "ActivationRequestEntity.findByPaId", query = "SELECT a FROM ActivationRequestEntity a WHERE a.paId = :paId")
    , @NamedQuery(name = "ActivationRequestEntity.findByPaStatus", query = "SELECT a FROM ActivationRequestEntity a WHERE a.paStatus = :paStatus")
    , @NamedQuery(name = "ActivationRequestEntity.findByPsId", query = "SELECT a FROM ActivationRequestEntity a WHERE a.psId = :psId")
    , @NamedQuery(name = "ActivationRequestEntity.findByBaaId", query = "SELECT a FROM ActivationRequestEntity a WHERE a.baaId = :baaId")
    , @NamedQuery(name = "ActivationRequestEntity.findByAlNo", query = "SELECT a FROM ActivationRequestEntity a WHERE a.alNo = :alNo")
    , @NamedQuery(name = "ActivationRequestEntity.findByAlStatus", query = "SELECT a FROM ActivationRequestEntity a WHERE a.alStatus = :alStatus")
    , @NamedQuery(name = "ActivationRequestEntity.findByAlReferenceNo", query = "SELECT a FROM ActivationRequestEntity a WHERE a.alReferenceNo = :alReferenceNo")
    , @NamedQuery(name = "ActivationRequestEntity.findByPriceLama", query = "SELECT a FROM ActivationRequestEntity a WHERE a.priceLama = :priceLama")
    , @NamedQuery(name = "ActivationRequestEntity.findBySidLama", query = "SELECT a FROM ActivationRequestEntity a WHERE a.sidLama = :sidLama")
    , @NamedQuery(name = "ActivationRequestEntity.findByCreatedon", query = "SELECT a FROM ActivationRequestEntity a WHERE a.createdon = :createdon")})
    @ToString(includeFieldNames=true)
public class ActivationRequestEntity implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "AR_NO")
    private String arNo;
    @Size(max = 30)
    @Column(name = "AR_TYPE")
    private String arType;
    @Size(max = 300)
    @Column(name = "CUST_NAME")
    private String custName;
    @Size(max = 100)
    @Column(name = "SEGMENT")
    private String segment;
    @Size(max = 30)
    @Column(name = "AR_STATUS")
    private String arStatus;
    @Column(name = "AR_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date arDate;
    @Column(name = "BA_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date baDate;
    @Column(name = "CANCEL_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date cancelDate;
    @Column(name = "APPROVAL_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date approvalDate;
    @Column(name = "TARGETDATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date targetdate;
    @Column(name = "CLOSEDATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date closedate;
    @Column(name = "ACTIVATIONACTUALDATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date activationactualdate;
    @Column(name = "DEACTIVATIONDATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date deactivationdate;
    @Size(max = 100)
    @Column(name = "SID")
    private String sid;
    @Size(max = 100)
    @Column(name = "LAYANAN")
    private String layanan;
    @Column(name = "QTY")
    private BigInteger qty;
    @Size(max = 20)
    @Column(name = "BANDWIDTH")
    private String bandwidth;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "BIAYA_SEWA")
    private Double biayaSewa;
    @Column(name = "BIAYA_INSTALASI")
    private Double biayaInstalasi;
    @Column(name = "BIAYA_RELOKASI")
    private Double biayaRelokasi;
    @Size(max = 500)
    @Column(name = "ADDRESS_ORIGINATING")
    private String addressOriginating;
    @Size(max = 100)
    @Column(name = "SBU_ORI")
    private String sbuOri;
    @Size(max = 500)
    @Column(name = "ADDRESS_TERMINATING")
    private String addressTerminating;
    @Size(max = 100)
    @Column(name = "SBU_TER")
    private String sbuTer;
    @Size(max = 50)
    @Column(name = "OWNER")
    private String owner;
    @Size(max = 100)
    @Column(name = "SBU_OWNER")
    private String sbuOwner;
    @Size(max = 100)
    @Column(name = "AGR_ID")
    private String agrId;
    @Size(max = 100)
    @Column(name = "PA_ID")
    private String paId;
    @Size(max = 30)
    @Column(name = "PA_STATUS")
    private String paStatus;
    @Size(max = 100)
    @Column(name = "PS_ID")
    private String psId;
    @Size(max = 100)
    @Column(name = "BAA_ID")
    private String baaId;
    @Size(max = 100)
    @Column(name = "AL_NO")
    private String alNo;
    @Size(max = 30)
    @Column(name = "AL_STATUS")
    private String alStatus;
    @Size(max = 100)
    @Column(name = "AL_REFERENCE_NO")
    private String alReferenceNo;
    @Column(name = "PRICE_LAMA")
    private Double priceLama;
    @Size(max = 100)
    @Column(name = "SID_LAMA")
    private String sidLama;
    @Column(name = "CREATEDON")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdon;

    public ActivationRequestEntity() {
    }

    public ActivationRequestEntity(String arNo) {
        this.arNo = arNo;
    }

    public String getArNo() {
        return arNo;
    }

    public void setArNo(String arNo) {
        this.arNo = arNo;
    }

    public String getArType() {
        return arType;
    }

    public void setArType(String arType) {
        this.arType = arType;
    }

    public String getCustName() {
        return custName;
    }

    public void setCustName(String custName) {
        this.custName = custName;
    }

    public String getSegment() {
        return segment;
    }

    public void setSegment(String segment) {
        this.segment = segment;
    }

    public String getArStatus() {
        return arStatus;
    }

    public void setArStatus(String arStatus) {
        this.arStatus = arStatus;
    }

    public Date getArDate() {
        return arDate;
    }

    public void setArDate(Date arDate) {
        this.arDate = arDate;
    }

    public Date getBaDate() {
        return baDate;
    }

    public void setBaDate(Date baDate) {
        this.baDate = baDate;
    }

    public Date getCancelDate() {
        return cancelDate;
    }

    public void setCancelDate(Date cancelDate) {
        this.cancelDate = cancelDate;
    }

    public Date getApprovalDate() {
        return approvalDate;
    }

    public void setApprovalDate(Date approvalDate) {
        this.approvalDate = approvalDate;
    }

    public Date getTargetdate() {
        return targetdate;
    }

    public void setTargetdate(Date targetdate) {
        this.targetdate = targetdate;
    }

    public Date getClosedate() {
        return closedate;
    }

    public void setClosedate(Date closedate) {
        this.closedate = closedate;
    }

    public Date getActivationactualdate() {
        return activationactualdate;
    }

    public void setActivationactualdate(Date activationactualdate) {
        this.activationactualdate = activationactualdate;
    }

    public Date getDeactivationdate() {
        return deactivationdate;
    }

    public void setDeactivationdate(Date deactivationdate) {
        this.deactivationdate = deactivationdate;
    }

    public String getSid() {
        return sid;
    }

    public void setSid(String sid) {
        this.sid = sid;
    }

    public String getLayanan() {
        return layanan;
    }

    public void setLayanan(String layanan) {
        this.layanan = layanan;
    }

    public BigInteger getQty() {
        return qty;
    }

    public void setQty(BigInteger qty) {
        this.qty = qty;
    }

    public String getBandwidth() {
        return bandwidth;
    }

    public void setBandwidth(String bandwidth) {
        this.bandwidth = bandwidth;
    }

    public Double getBiayaSewa() {
        return biayaSewa;
    }

    public void setBiayaSewa(Double biayaSewa) {
        this.biayaSewa = biayaSewa;
    }

    public Double getBiayaInstalasi() {
        return biayaInstalasi;
    }

    public void setBiayaInstalasi(Double biayaInstalasi) {
        this.biayaInstalasi = biayaInstalasi;
    }

    public Double getBiayaRelokasi() {
        return biayaRelokasi;
    }

    public void setBiayaRelokasi(Double biayaRelokasi) {
        this.biayaRelokasi = biayaRelokasi;
    }

    public String getAddressOriginating() {
        return addressOriginating;
    }

    public void setAddressOriginating(String addressOriginating) {
        this.addressOriginating = addressOriginating;
    }

    public String getSbuOri() {
        return sbuOri;
    }

    public void setSbuOri(String sbuOri) {
        this.sbuOri = sbuOri;
    }

    public String getAddressTerminating() {
        return addressTerminating;
    }

    public void setAddressTerminating(String addressTerminating) {
        this.addressTerminating = addressTerminating;
    }

    public String getSbuTer() {
        return sbuTer;
    }

    public void setSbuTer(String sbuTer) {
        this.sbuTer = sbuTer;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getSbuOwner() {
        return sbuOwner;
    }

    public void setSbuOwner(String sbuOwner) {
        this.sbuOwner = sbuOwner;
    }

    public String getAgrId() {
        return agrId;
    }

    public void setAgrId(String agrId) {
        this.agrId = agrId;
    }

    public String getPaId() {
        return paId;
    }

    public void setPaId(String paId) {
        this.paId = paId;
    }

    public String getPaStatus() {
        return paStatus;
    }

    public void setPaStatus(String paStatus) {
        this.paStatus = paStatus;
    }

    public String getPsId() {
        return psId;
    }

    public void setPsId(String psId) {
        this.psId = psId;
    }

    public String getBaaId() {
        return baaId;
    }

    public void setBaaId(String baaId) {
        this.baaId = baaId;
    }

    public String getAlNo() {
        return alNo;
    }

    public void setAlNo(String alNo) {
        this.alNo = alNo;
    }

    public String getAlStatus() {
        return alStatus;
    }

    public void setAlStatus(String alStatus) {
        this.alStatus = alStatus;
    }

    public String getAlReferenceNo() {
        return alReferenceNo;
    }

    public void setAlReferenceNo(String alReferenceNo) {
        this.alReferenceNo = alReferenceNo;
    }

    public Double getPriceLama() {
        return priceLama;
    }

    public void setPriceLama(Double priceLama) {
        this.priceLama = priceLama;
    }

    public String getSidLama() {
        return sidLama;
    }

    public void setSidLama(String sidLama) {
        this.sidLama = sidLama;
    }

    public Date getCreatedon() {
        return createdon;
    }

    public void setCreatedon(Date createdon) {
        this.createdon = createdon;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (arNo != null ? arNo.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ActivationRequestEntity)) {
            return false;
        }
        ActivationRequestEntity other = (ActivationRequestEntity) object;
        if ((this.arNo == null && other.arNo != null) || (this.arNo != null && !this.arNo.equals(other.arNo))) {
            return false;
        }
        return true;
    }


}
