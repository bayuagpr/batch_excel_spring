/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.spkk.backend.model;

import lombok.ToString;

import java.io.Serializable;
import java.math.BigInteger;
import javax.persistence.*;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author bayua
 */
@Entity
@Table(name = "V_AL_EST_DEC")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "VAlEstDecView.findAll", query = "SELECT v FROM VAlEstDecView v")
    , @NamedQuery(name = "VAlEstDecView.findByGroupOwner", query = "SELECT v FROM VAlEstDecView v WHERE v.groupOwner = :groupOwner")
    , @NamedQuery(name = "VAlEstDecView.findByCoTahunSebelumnya", query = "SELECT v FROM VAlEstDecView v WHERE v.coTahunSebelumnya = :coTahunSebelumnya")
    , @NamedQuery(name = "VAlEstDecView.findByNrTahunIni", query = "SELECT v FROM VAlEstDecView v WHERE v.nrTahunIni = :nrTahunIni")
    , @NamedQuery(name = "VAlEstDecView.findByTotal", query = "SELECT v FROM VAlEstDecView v WHERE v.total = :total")})
@ToString(includeFieldNames=true)
public class VAlEstDecView implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Size(max = 50)
    @Column(name = "GROUP_OWNER")
    private String groupOwner;
    @Column(name = "CO_TAHUN_SEBELUMNYA")
    private BigInteger coTahunSebelumnya;
    @Column(name = "NR_TAHUN_INI")
    private BigInteger nrTahunIni;
    @Column(name = "TOTAL")
    private BigInteger total;

    public VAlEstDecView() {
    }

    public String getGroupOwner() {
        return groupOwner;
    }

    public void setGroupOwner(String groupOwner) {
        this.groupOwner = groupOwner;
    }

    public BigInteger getCoTahunSebelumnya() {
        return coTahunSebelumnya;
    }

    public void setCoTahunSebelumnya(BigInteger coTahunSebelumnya) {
        this.coTahunSebelumnya = coTahunSebelumnya;
    }

    public BigInteger getNrTahunIni() {
        return nrTahunIni;
    }

    public void setNrTahunIni(BigInteger nrTahunIni) {
        this.nrTahunIni = nrTahunIni;
    }

    public BigInteger getTotal() {
        return total;
    }

    public void setTotal(BigInteger total) {
        this.total = total;
    }
    
}
