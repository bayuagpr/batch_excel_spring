/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.spkk.backend.model;

import lombok.ToString;

import java.io.Serializable;
import java.math.BigInteger;
import javax.persistence.*;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author bayua
 */
@Entity
@Table(name = "V_SUM_GO_PA")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "VSumGoPaView.findAll", query = "SELECT v FROM VSumGoPaView v")
    , @NamedQuery(name = "VSumGoPaView.findByGroupOwner", query = "SELECT v FROM VSumGoPaView v WHERE v.groupOwner = :groupOwner")
    , @NamedQuery(name = "VSumGoPaView.findByVol", query = "SELECT v FROM VSumGoPaView v WHERE v.vol = :vol")
    , @NamedQuery(name = "VSumGoPaView.findByOtc", query = "SELECT v FROM VSumGoPaView v WHERE v.otc = :otc")
    , @NamedQuery(name = "VSumGoPaView.findBySewa", query = "SELECT v FROM VSumGoPaView v WHERE v.sewa = :sewa")
    , @NamedQuery(name = "VSumGoPaView.findByTotal", query = "SELECT v FROM VSumGoPaView v WHERE v.total = :total")})
@ToString(includeFieldNames=true)
public class VSumGoPaView implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Size(max = 100)
    @Column(name = "GROUP_OWNER")
    private String groupOwner;
    @Column(name = "VOL")
    private BigInteger vol;
    @Column(name = "OTC")
    private BigInteger otc;
    @Column(name = "SEWA")
    private BigInteger sewa;
    @Column(name = "TOTAL")
    private BigInteger total;

    public VSumGoPaView() {
    }

    public String getGroupOwner() {
        return groupOwner;
    }

    public void setGroupOwner(String groupOwner) {
        this.groupOwner = groupOwner;
    }

    public BigInteger getVol() {
        return vol;
    }

    public void setVol(BigInteger vol) {
        this.vol = vol;
    }

    public BigInteger getOtc() {
        return otc;
    }

    public void setOtc(BigInteger otc) {
        this.otc = otc;
    }

    public BigInteger getSewa() {
        return sewa;
    }

    public void setSewa(BigInteger sewa) {
        this.sewa = sewa;
    }

    public BigInteger getTotal() {
        return total;
    }

    public void setTotal(BigInteger total) {
        this.total = total;
    }
    
}
