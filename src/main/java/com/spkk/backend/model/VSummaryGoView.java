/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.spkk.backend.model;

import lombok.ToString;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.*;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author bayua
 */
@Entity
@Table(name = "V_SUMMARY_GO")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "VSummaryGoView.findAll", query = "SELECT v FROM VSummaryGoView v")
    , @NamedQuery(name = "VSummaryGoView.findByGroupOwner", query = "SELECT v FROM VSummaryGoView v WHERE v.groupOwner = :groupOwner")
    , @NamedQuery(name = "VSummaryGoView.findByVolBa", query = "SELECT v FROM VSummaryGoView v WHERE v.volBa = :volBa")
    , @NamedQuery(name = "VSummaryGoView.findByTotalBa", query = "SELECT v FROM VSummaryGoView v WHERE v.totalBa = :totalBa")
    , @NamedQuery(name = "VSummaryGoView.findByVolPa", query = "SELECT v FROM VSummaryGoView v WHERE v.volPa = :volPa")
    , @NamedQuery(name = "VSummaryGoView.findByTotalPa", query = "SELECT v FROM VSummaryGoView v WHERE v.totalPa = :totalPa")
    , @NamedQuery(name = "VSummaryGoView.findByTotalRevenue", query = "SELECT v FROM VSummaryGoView v WHERE v.totalRevenue = :totalRevenue")})
@ToString(includeFieldNames=true)
public class VSummaryGoView implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Size(max = 100)
    @Column(name = "GROUP_OWNER")
    private String groupOwner;
    @Column(name = "VOL_BA")
    private BigDecimal volBa;
    @Column(name = "TOTAL_BA")
    private BigDecimal totalBa;
    @Column(name = "VOL_PA")
    private BigDecimal volPa;
    @Column(name = "TOTAL_PA")
    private BigDecimal totalPa;
    @Column(name = "TOTAL_REVENUE")
    private BigDecimal totalRevenue;

    public VSummaryGoView() {
    }

    public String getGroupOwner() {
        return groupOwner;
    }

    public void setGroupOwner(String groupOwner) {
        this.groupOwner = groupOwner;
    }

    public BigDecimal getVolBa() {
        return volBa;
    }

    public void setVolBa(BigDecimal volBa) {
        this.volBa = volBa;
    }

    public BigDecimal getTotalBa() {
        return totalBa;
    }

    public void setTotalBa(BigDecimal totalBa) {
        this.totalBa = totalBa;
    }

    public BigDecimal getVolPa() {
        return volPa;
    }

    public void setVolPa(BigDecimal volPa) {
        this.volPa = volPa;
    }

    public BigDecimal getTotalPa() {
        return totalPa;
    }

    public void setTotalPa(BigDecimal totalPa) {
        this.totalPa = totalPa;
    }

    public BigDecimal getTotalRevenue() {
        return totalRevenue;
    }

    public void setTotalRevenue(BigDecimal totalRevenue) {
        this.totalRevenue = totalRevenue;
    }
    
}
