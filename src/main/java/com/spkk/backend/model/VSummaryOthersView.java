/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.spkk.backend.model;

import lombok.ToString;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.*;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author bayua
 */
@Entity
@Table(name = "V_SUMMARY_OTHERS")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "VSummaryOthersView.findAll", query = "SELECT v FROM VSummaryOthersView v")
    , @NamedQuery(name = "VSummaryOthersView.findByGroupOwner", query = "SELECT v FROM VSummaryOthersView v WHERE v.groupOwner = :groupOwner")
    , @NamedQuery(name = "VSummaryOthersView.findByTarget", query = "SELECT v FROM VSummaryOthersView v WHERE v.target = :target")
    , @NamedQuery(name = "VSummaryOthersView.findByCoNr", query = "SELECT v FROM VSummaryOthersView v WHERE v.coNr = :coNr")
    , @NamedQuery(name = "VSummaryOthersView.findByPotensiSoClosed", query = "SELECT v FROM VSummaryOthersView v WHERE v.potensiSoClosed = :potensiSoClosed")
    , @NamedQuery(name = "VSummaryOthersView.findByPotensiSoProgress", query = "SELECT v FROM VSummaryOthersView v WHERE v.potensiSoProgress = :potensiSoProgress")
    , @NamedQuery(name = "VSummaryOthersView.findByPotensiSoOpen", query = "SELECT v FROM VSummaryOthersView v WHERE v.potensiSoOpen = :potensiSoOpen")
    , @NamedQuery(name = "VSummaryOthersView.findByTotal", query = "SELECT v FROM VSummaryOthersView v WHERE v.total = :total")
    , @NamedQuery(name = "VSummaryOthersView.findByPersenTarget", query = "SELECT v FROM VSummaryOthersView v WHERE v.persenTarget = :persenTarget")})
@ToString(includeFieldNames=true)
public class VSummaryOthersView implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Size(max = 100)
    @Column(name = "GROUP_OWNER")
    private String groupOwner;
    @Column(name = "TARGET")
    private BigDecimal target;
    @Column(name = "CO_NR")
    private BigDecimal coNr;
    @Column(name = "POTENSI_SO_CLOSED")
    private BigDecimal potensiSoClosed;
    @Column(name = "POTENSI_SO_PROGRESS")
    private BigDecimal potensiSoProgress;
    @Column(name = "POTENSI_SO_OPEN")
    private BigDecimal potensiSoOpen;
    @Column(name = "TOTAL")
    private BigDecimal total;
    @Size(max = 22)
    @Column(name = "PERSEN_TARGET")
    private String persenTarget;

    public VSummaryOthersView() {
    }

    public String getGroupOwner() {
        return groupOwner;
    }

    public void setGroupOwner(String groupOwner) {
        this.groupOwner = groupOwner;
    }

    public BigDecimal getTarget() {
        return target;
    }

    public void setTarget(BigDecimal target) {
        this.target = target;
    }

    public BigDecimal getCoNr() {
        return coNr;
    }

    public void setCoNr(BigDecimal coNr) {
        this.coNr = coNr;
    }

    public BigDecimal getPotensiSoClosed() {
        return potensiSoClosed;
    }

    public void setPotensiSoClosed(BigDecimal potensiSoClosed) {
        this.potensiSoClosed = potensiSoClosed;
    }

    public BigDecimal getPotensiSoProgress() {
        return potensiSoProgress;
    }

    public void setPotensiSoProgress(BigDecimal potensiSoProgress) {
        this.potensiSoProgress = potensiSoProgress;
    }

    public BigDecimal getPotensiSoOpen() {
        return potensiSoOpen;
    }

    public void setPotensiSoOpen(BigDecimal potensiSoOpen) {
        this.potensiSoOpen = potensiSoOpen;
    }

    public BigDecimal getTotal() {
        return total;
    }

    public void setTotal(BigDecimal total) {
        this.total = total;
    }

    public String getPersenTarget() {
        return persenTarget;
    }

    public void setPersenTarget(String persenTarget) {
        this.persenTarget = persenTarget;
    }
    
}
