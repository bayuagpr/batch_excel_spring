
package com.spkk.backend.model;

import lombok.ToString;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name = "ACTIVATION_LIST", schema = "WAHYUTONI")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ActivationListEntity.findAll", query = "SELECT a FROM ActivationListEntity a")
    , @NamedQuery(name = "ActivationListEntity.findByAlNo", query = "SELECT a FROM ActivationListEntity a WHERE a.alNo = :alNo")
    , @NamedQuery(name = "ActivationListEntity.findByAlStatus", query = "SELECT a FROM ActivationListEntity a WHERE a.alStatus = :alStatus")
    , @NamedQuery(name = "ActivationListEntity.findByAlType", query = "SELECT a FROM ActivationListEntity a WHERE a.alType = :alType")
    , @NamedQuery(name = "ActivationListEntity.findByArNo", query = "SELECT a FROM ActivationListEntity a WHERE a.arNo = :arNo")
    , @NamedQuery(name = "ActivationListEntity.findByArStatus", query = "SELECT a FROM ActivationListEntity a WHERE a.arStatus = :arStatus")
    , @NamedQuery(name = "ActivationListEntity.findByPaId", query = "SELECT a FROM ActivationListEntity a WHERE a.paId = :paId")
    , @NamedQuery(name = "ActivationListEntity.findByPaStatus", query = "SELECT a FROM ActivationListEntity a WHERE a.paStatus = :paStatus")
    , @NamedQuery(name = "ActivationListEntity.findByCustid", query = "SELECT a FROM ActivationListEntity a WHERE a.custid = :custid")
    , @NamedQuery(name = "ActivationListEntity.findByCustname", query = "SELECT a FROM ActivationListEntity a WHERE a.custname = :custname")
    , @NamedQuery(name = "ActivationListEntity.findBySegment", query = "SELECT a FROM ActivationListEntity a WHERE a.segment = :segment")
    , @NamedQuery(name = "ActivationListEntity.findBySid", query = "SELECT a FROM ActivationListEntity a WHERE a.sid = :sid")
    , @NamedQuery(name = "ActivationListEntity.findByAddressOri", query = "SELECT a FROM ActivationListEntity a WHERE a.addressOri = :addressOri")
    , @NamedQuery(name = "ActivationListEntity.findBySbuOri", query = "SELECT a FROM ActivationListEntity a WHERE a.sbuOri = :sbuOri")
    , @NamedQuery(name = "ActivationListEntity.findByAddressTer", query = "SELECT a FROM ActivationListEntity a WHERE a.addressTer = :addressTer")
    , @NamedQuery(name = "ActivationListEntity.findBySbuTer", query = "SELECT a FROM ActivationListEntity a WHERE a.sbuTer = :sbuTer")
    , @NamedQuery(name = "ActivationListEntity.findByAgrNo", query = "SELECT a FROM ActivationListEntity a WHERE a.agrNo = :agrNo")
    , @NamedQuery(name = "ActivationListEntity.findByLayanan", query = "SELECT a FROM ActivationListEntity a WHERE a.layanan = :layanan")
    , @NamedQuery(name = "ActivationListEntity.findByBandwidth", query = "SELECT a FROM ActivationListEntity a WHERE a.bandwidth = :bandwidth")
    , @NamedQuery(name = "ActivationListEntity.findByQty", query = "SELECT a FROM ActivationListEntity a WHERE a.qty = :qty")
    , @NamedQuery(name = "ActivationListEntity.findByBiayaSewa", query = "SELECT a FROM ActivationListEntity a WHERE a.biayaSewa = :biayaSewa")
    , @NamedQuery(name = "ActivationListEntity.findByBiayaInstalasi", query = "SELECT a FROM ActivationListEntity a WHERE a.biayaInstalasi = :biayaInstalasi")
    , @NamedQuery(name = "ActivationListEntity.findByBiayaRelokasi", query = "SELECT a FROM ActivationListEntity a WHERE a.biayaRelokasi = :biayaRelokasi")
    , @NamedQuery(name = "ActivationListEntity.findByBiayaSewaLama", query = "SELECT a FROM ActivationListEntity a WHERE a.biayaSewaLama = :biayaSewaLama")
    , @NamedQuery(name = "ActivationListEntity.findByAlReference", query = "SELECT a FROM ActivationListEntity a WHERE a.alReference = :alReference")
    , @NamedQuery(name = "ActivationListEntity.findByStartbilldate", query = "SELECT a FROM ActivationListEntity a WHERE a.startbilldate = :startbilldate")
    , @NamedQuery(name = "ActivationListEntity.findByLastinvoicedate", query = "SELECT a FROM ActivationListEntity a WHERE a.lastinvoicedate = :lastinvoicedate")
    , @NamedQuery(name = "ActivationListEntity.findByDeactivationdate", query = "SELECT a FROM ActivationListEntity a WHERE a.deactivationdate = :deactivationdate")
    , @NamedQuery(name = "ActivationListEntity.findByOwner", query = "SELECT a FROM ActivationListEntity a WHERE a.owner = :owner")
    , @NamedQuery(name = "ActivationListEntity.findBySbuOwner", query = "SELECT a FROM ActivationListEntity a WHERE a.sbuOwner = :sbuOwner")
    , @NamedQuery(name = "ActivationListEntity.findByCreatedon", query = "SELECT a FROM ActivationListEntity a WHERE a.createdon = :createdon")})
    @ToString(includeFieldNames=true)
public class ActivationListEntity implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "AL_NO")
    private String alNo;
    @Size(max = 30)
    @Column(name = "AL_STATUS")
    private String alStatus;
    @Size(max = 30)
    @Column(name = "AL_TYPE")
    private String alType;
    @Size(max = 100)
    @Column(name = "AR_NO")
    private String arNo;
    @Size(max = 30)
    @Column(name = "AR_STATUS")
    private String arStatus;
    @Size(max = 100)
    @Column(name = "PA_ID")
    private String paId;
    @Size(max = 30)
    @Column(name = "PA_STATUS")
    private String paStatus;
    @Size(max = 100)
    @Column(name = "CUSTID")
    private String custid;
    @Size(max = 300)
    @Column(name = "CUSTNAME")
    private String custname;
    @Size(max = 100)
    @Column(name = "SEGMENT")
    private String segment;
    @Size(max = 100)
    @Column(name = "SID")
    private String sid;
    @Size(max = 500)
    @Column(name = "ADDRESS_ORI")
    private String addressOri;
    @Size(max = 100)
    @Column(name = "SBU_ORI")
    private String sbuOri;
    @Size(max = 500)
    @Column(name = "ADDRESS_TER")
    private String addressTer;
    @Size(max = 100)
    @Column(name = "SBU_TER")
    private String sbuTer;
    @Size(max = 100)
    @Column(name = "AGR_NO")
    private String agrNo;
    @Size(max = 100)
    @Column(name = "LAYANAN")
    private String layanan;
    @Size(max = 20)
    @Column(name = "BANDWIDTH")
    private String bandwidth;
    @Column(name = "QTY")
    private BigInteger qty;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "BIAYA_SEWA")
    private Double biayaSewa;
    @Column(name = "BIAYA_INSTALASI")
    private Double biayaInstalasi;
    @Column(name = "BIAYA_RELOKASI")
    private Double biayaRelokasi;
    @Column(name = "BIAYA_SEWA_LAMA")
    private Double biayaSewaLama;
    @Size(max = 100)
    @Column(name = "AL_REFERENCE")
    private String alReference;
    @Column(name = "STARTBILLDATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date startbilldate;
    @Column(name = "LASTINVOICEDATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastinvoicedate;
    @Column(name = "DEACTIVATIONDATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date deactivationdate;
    @Size(max = 50)
    @Column(name = "OWNER")
    private String owner;
    @Size(max = 100)
    @Column(name = "SBU_OWNER")
    private String sbuOwner;
    @Column(name = "CREATEDON")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdon;

    public ActivationListEntity() {
    }

    public ActivationListEntity(String alNo) {
        this.alNo = alNo;
    }

    public String getAlNo() {
        return alNo;
    }

    public void setAlNo(String alNo) {
        this.alNo = alNo;
    }

    public String getAlStatus() {
        return alStatus;
    }

    public void setAlStatus(String alStatus) {
        this.alStatus = alStatus;
    }

    public String getAlType() {
        return alType;
    }

    public void setAlType(String alType) {
        this.alType = alType;
    }

    public String getArNo() {
        return arNo;
    }

    public void setArNo(String arNo) {
        this.arNo = arNo;
    }

    public String getArStatus() {
        return arStatus;
    }

    public void setArStatus(String arStatus) {
        this.arStatus = arStatus;
    }

    public String getPaId() {
        return paId;
    }

    public void setPaId(String paId) {
        this.paId = paId;
    }

    public String getPaStatus() {
        return paStatus;
    }

    public void setPaStatus(String paStatus) {
        this.paStatus = paStatus;
    }

    public String getCustid() {
        return custid;
    }

    public void setCustid(String custid) {
        this.custid = custid;
    }

    public String getCustname() {
        return custname;
    }

    public void setCustname(String custname) {
        this.custname = custname;
    }

    public String getSegment() {
        return segment;
    }

    public void setSegment(String segment) {
        this.segment = segment;
    }

    public String getSid() {
        return sid;
    }

    public void setSid(String sid) {
        this.sid = sid;
    }

    public String getAddressOri() {
        return addressOri;
    }

    public void setAddressOri(String addressOri) {
        this.addressOri = addressOri;
    }

    public String getSbuOri() {
        return sbuOri;
    }

    public void setSbuOri(String sbuOri) {
        this.sbuOri = sbuOri;
    }

    public String getAddressTer() {
        return addressTer;
    }

    public void setAddressTer(String addressTer) {
        this.addressTer = addressTer;
    }

    public String getSbuTer() {
        return sbuTer;
    }

    public void setSbuTer(String sbuTer) {
        this.sbuTer = sbuTer;
    }

    public String getAgrNo() {
        return agrNo;
    }

    public void setAgrNo(String agrNo) {
        this.agrNo = agrNo;
    }

    public String getLayanan() {
        return layanan;
    }

    public void setLayanan(String layanan) {
        this.layanan = layanan;
    }

    public String getBandwidth() {
        return bandwidth;
    }

    public void setBandwidth(String bandwidth) {
        this.bandwidth = bandwidth;
    }

    public BigInteger getQty() {
        return qty;
    }

    public void setQty(BigInteger qty) {
        this.qty = qty;
    }

    public Double getBiayaSewa() {
        return biayaSewa;
    }

    public void setBiayaSewa(Double biayaSewa) {
        this.biayaSewa = biayaSewa;
    }

    public Double getBiayaInstalasi() {
        return biayaInstalasi;
    }

    public void setBiayaInstalasi(Double biayaInstalasi) {
        this.biayaInstalasi = biayaInstalasi;
    }

    public Double getBiayaRelokasi() {
        return biayaRelokasi;
    }

    public void setBiayaRelokasi(Double biayaRelokasi) {
        this.biayaRelokasi = biayaRelokasi;
    }

    public Double getBiayaSewaLama() {
        return biayaSewaLama;
    }

    public void setBiayaSewaLama(Double biayaSewaLama) {
        this.biayaSewaLama = biayaSewaLama;
    }

    public String getAlReference() {
        return alReference;
    }

    public void setAlReference(String alReference) {
        this.alReference = alReference;
    }

    public Date getStartbilldate() {
        return startbilldate;
    }

    public void setStartbilldate(Date startbilldate) {
        this.startbilldate = startbilldate;
    }

    public Date getLastinvoicedate() {
        return lastinvoicedate;
    }

    public void setLastinvoicedate(Date lastinvoicedate) {
        this.lastinvoicedate = lastinvoicedate;
    }

    public Date getDeactivationdate() {
        return deactivationdate;
    }

    public void setDeactivationdate(Date deactivationdate) {
        this.deactivationdate = deactivationdate;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getSbuOwner() {
        return sbuOwner;
    }

    public void setSbuOwner(String sbuOwner) {
        this.sbuOwner = sbuOwner;
    }

    public Date getCreatedon() {
        return createdon;
    }

    public void setCreatedon(Date createdon) {
        this.createdon = createdon;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (alNo != null ? alNo.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ActivationListEntity)) {
            return false;
        }
        ActivationListEntity other = (ActivationListEntity) object;
        if ((this.alNo == null && other.alNo != null) || (this.alNo != null && !this.alNo.equals(other.alNo))) {
            return false;
        }
        return true;
    }


    
}
