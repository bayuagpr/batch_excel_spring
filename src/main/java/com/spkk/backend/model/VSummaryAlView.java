/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.spkk.backend.model;

import lombok.ToString;

import java.io.Serializable;
import java.math.BigInteger;
import javax.persistence.*;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author bayua
 */
@Entity
@Table(name = "V_SUMMARY_AL")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "VSummaryAlView.findAll", query = "SELECT v FROM VSummaryAlView v")
    , @NamedQuery(name = "VSummaryAlView.findByGroupOwner", query = "SELECT v FROM VSummaryAlView v WHERE v.groupOwner = :groupOwner")
    , @NamedQuery(name = "VSummaryAlView.findByCo2017Juni", query = "SELECT v FROM VSummaryAlView v WHERE v.co2017Juni = :co2017Juni")
    , @NamedQuery(name = "VSummaryAlView.findByNr2018Juni", query = "SELECT v FROM VSummaryAlView v WHERE v.nr2018Juni = :nr2018Juni")
    , @NamedQuery(name = "VSummaryAlView.findByTotalJuni", query = "SELECT v FROM VSummaryAlView v WHERE v.totalJuni = :totalJuni")
    , @NamedQuery(name = "VSummaryAlView.findByCo2017Est", query = "SELECT v FROM VSummaryAlView v WHERE v.co2017Est = :co2017Est")
    , @NamedQuery(name = "VSummaryAlView.findByNr2018Est", query = "SELECT v FROM VSummaryAlView v WHERE v.nr2018Est = :nr2018Est")
    , @NamedQuery(name = "VSummaryAlView.findByTotalEst", query = "SELECT v FROM VSummaryAlView v WHERE v.totalEst = :totalEst")
    , @NamedQuery(name = "VSummaryAlView.findByTargetJuni2018", query = "SELECT v FROM VSummaryAlView v WHERE v.targetJuni2018 = :targetJuni2018")
    , @NamedQuery(name = "VSummaryAlView.findByRealisasiTargetJuni", query = "SELECT v FROM VSummaryAlView v WHERE v.realisasiTargetJuni = :realisasiTargetJuni")
    , @NamedQuery(name = "VSummaryAlView.findByTarget2018", query = "SELECT v FROM VSummaryAlView v WHERE v.target2018 = :target2018")
    , @NamedQuery(name = "VSummaryAlView.findByRealisasiEstimasiTarget", query = "SELECT v FROM VSummaryAlView v WHERE v.realisasiEstimasiTarget = :realisasiEstimasiTarget")})
@ToString(includeFieldNames=true)
public class VSummaryAlView implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Size(max = 50)
    @Column(name = "GROUP_OWNER")
    private String groupOwner;
    @Column(name = "CO_2017_JUNI")
    private BigInteger co2017Juni;
    @Column(name = "NR_2018_JUNI")
    private BigInteger nr2018Juni;
    @Column(name = "TOTAL_JUNI")
    private BigInteger totalJuni;
    @Column(name = "CO_2017_EST")
    private BigInteger co2017Est;
    @Column(name = "NR_2018_EST")
    private BigInteger nr2018Est;
    @Column(name = "TOTAL_EST")
    private BigInteger totalEst;
    @Column(name = "TARGET_JUNI_2018")
    private BigInteger targetJuni2018;
    @Size(max = 22)
    @Column(name = "REALISASI_TARGET_JUNI")
    private String realisasiTargetJuni;
    @Column(name = "TARGET_2018")
    private BigInteger target2018;
    @Size(max = 22)
    @Column(name = "REALISASI_ESTIMASI_TARGET")
    private String realisasiEstimasiTarget;

    public VSummaryAlView() {
    }

    public String getGroupOwner() {
        return groupOwner;
    }

    public void setGroupOwner(String groupOwner) {
        this.groupOwner = groupOwner;
    }

    public BigInteger getCo2017Juni() {
        return co2017Juni;
    }

    public void setCo2017Juni(BigInteger co2017Juni) {
        this.co2017Juni = co2017Juni;
    }

    public BigInteger getNr2018Juni() {
        return nr2018Juni;
    }

    public void setNr2018Juni(BigInteger nr2018Juni) {
        this.nr2018Juni = nr2018Juni;
    }

    public BigInteger getTotalJuni() {
        return totalJuni;
    }

    public void setTotalJuni(BigInteger totalJuni) {
        this.totalJuni = totalJuni;
    }

    public BigInteger getCo2017Est() {
        return co2017Est;
    }

    public void setCo2017Est(BigInteger co2017Est) {
        this.co2017Est = co2017Est;
    }

    public BigInteger getNr2018Est() {
        return nr2018Est;
    }

    public void setNr2018Est(BigInteger nr2018Est) {
        this.nr2018Est = nr2018Est;
    }

    public BigInteger getTotalEst() {
        return totalEst;
    }

    public void setTotalEst(BigInteger totalEst) {
        this.totalEst = totalEst;
    }

    public BigInteger getTargetJuni2018() {
        return targetJuni2018;
    }

    public void setTargetJuni2018(BigInteger targetJuni2018) {
        this.targetJuni2018 = targetJuni2018;
    }

    public String getRealisasiTargetJuni() {
        return realisasiTargetJuni;
    }

    public void setRealisasiTargetJuni(String realisasiTargetJuni) {
        this.realisasiTargetJuni = realisasiTargetJuni;
    }

    public BigInteger getTarget2018() {
        return target2018;
    }

    public void setTarget2018(BigInteger target2018) {
        this.target2018 = target2018;
    }

    public String getRealisasiEstimasiTarget() {
        return realisasiEstimasiTarget;
    }

    public void setRealisasiEstimasiTarget(String realisasiEstimasiTarget) {
        this.realisasiEstimasiTarget = realisasiEstimasiTarget;
    }
    
}
