/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.spkk.backend.model;

import lombok.ToString;

import java.io.Serializable;
import java.math.BigInteger;
import javax.persistence.*;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author bayua
 */
@Entity
@Table(name = "V_SUM_GL_PA")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "VSumGlPaView.findAll", query = "SELECT v FROM VSumGlPaView v")
    , @NamedQuery(name = "VSumGlPaView.findByGroupLayanan", query = "SELECT v FROM VSumGlPaView v WHERE v.groupLayanan = :groupLayanan")
    , @NamedQuery(name = "VSumGlPaView.findByVol", query = "SELECT v FROM VSumGlPaView v WHERE v.vol = :vol")
    , @NamedQuery(name = "VSumGlPaView.findByTotal", query = "SELECT v FROM VSumGlPaView v WHERE v.total = :total")})
@ToString(includeFieldNames=true)
public class VSumGlPaView implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Size(max = 100)
    @Column(name = "GROUP_LAYANAN")
    private String groupLayanan;
    @Column(name = "VOL")
    private BigInteger vol;
    @Column(name = "TOTAL")
    private BigInteger total;

    public VSumGlPaView() {
    }

    public String getGroupLayanan() {
        return groupLayanan;
    }

    public void setGroupLayanan(String groupLayanan) {
        this.groupLayanan = groupLayanan;
    }

    public BigInteger getVol() {
        return vol;
    }

    public void setVol(BigInteger vol) {
        this.vol = vol;
    }

    public BigInteger getTotal() {
        return total;
    }

    public void setTotal(BigInteger total) {
        this.total = total;
    }
    
}
