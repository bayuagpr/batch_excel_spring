/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.spkk.backend.model;

import lombok.ToString;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author bayua
 */
@Entity
@Table(name = "GROUP_LAYANAN")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "GroupLayananEntity.findAll", query = "SELECT g FROM GroupLayananEntity g")
    , @NamedQuery(name = "GroupLayananEntity.findByLayanan", query = "SELECT g FROM GroupLayananEntity g WHERE g.layanan = :layanan")
    , @NamedQuery(name = "GroupLayananEntity.findByGroupLayanan", query = "SELECT g FROM GroupLayananEntity g WHERE g.groupLayanan = :groupLayanan")
    , @NamedQuery(name = "GroupLayananEntity.findByJenis", query = "SELECT g FROM GroupLayananEntity g WHERE g.jenis = :jenis")
    , @NamedQuery(name = "GroupLayananEntity.findById", query = "SELECT g FROM GroupLayananEntity g WHERE g.groupLayananId = :groupLayananId")})
@ToString(includeFieldNames=true)
public class GroupLayananEntity implements Serializable {

    private static final long serialVersionUID = 1L;
    @Size(max = 100)
    @Column(name = "LAYANAN")
    private String layanan;
    @Size(max = 100)
    @Column(name = "GROUP_LAYANAN")
    private String groupLayanan;
    @Size(max = 20)
    @Column(name = "JENIS")
    private String jenis;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "GROUP_LAYANAN_ID")
    private BigDecimal groupLayananId;

    public GroupLayananEntity() {
    }

    public GroupLayananEntity(BigDecimal groupLayananId) {
        this.groupLayananId = groupLayananId;
    }

    public String getLayanan() {
        return layanan;
    }

    public void setLayanan(String layanan) {
        this.layanan = layanan;
    }

    public String getGroupLayanan() {
        return groupLayanan;
    }

    public void setGroupLayanan(String groupLayanan) {
        this.groupLayanan = groupLayanan;
    }

    public String getJenis() {
        return jenis;
    }

    public void setJenis(String jenis) {
        this.jenis = jenis;
    }

    public BigDecimal getGroupLayananId() {
        return groupLayananId;
    }

    public void setGroupLayananId(BigDecimal id) {
        this.groupLayananId = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (groupLayananId != null ? groupLayananId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof GroupLayananEntity)) {
            return false;
        }
        GroupLayananEntity other = (GroupLayananEntity) object;
        if ((this.groupLayananId == null && other.groupLayananId != null) || (this.groupLayananId != null && !this.groupLayananId.equals(other.groupLayananId))) {
            return false;
        }
        return true;
    }


}
