/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.spkk.backend.model;

import lombok.ToString;

import java.io.Serializable;
import java.math.BigInteger;
import javax.persistence.*;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author bayua
 */
@Entity
@Table(name = "V_SUM_GO_BA")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "VSumGoBaView.findAll", query = "SELECT v FROM VSumGoBaView v")
    , @NamedQuery(name = "VSumGoBaView.findByGroupOwner", query = "SELECT v FROM VSumGoBaView v WHERE v.groupOwner = :groupOwner")
    , @NamedQuery(name = "VSumGoBaView.findByVol", query = "SELECT v FROM VSumGoBaView v WHERE v.vol = :vol")
    , @NamedQuery(name = "VSumGoBaView.findByOtc", query = "SELECT v FROM VSumGoBaView v WHERE v.otc = :otc")
    , @NamedQuery(name = "VSumGoBaView.findBySewa", query = "SELECT v FROM VSumGoBaView v WHERE v.sewa = :sewa")
    , @NamedQuery(name = "VSumGoBaView.findByTotal", query = "SELECT v FROM VSumGoBaView v WHERE v.total = :total")})
@ToString(includeFieldNames=true)
public class VSumGoBaView implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Size(max = 100)
    @Column(name = "GROUP_OWNER")
    private String groupOwner;
    @Column(name = "VOL")
    private BigInteger vol;
    @Column(name = "OTC")
    private BigInteger otc;
    @Column(name = "SEWA")
    private BigInteger sewa;
    @Column(name = "TOTAL")
    private BigInteger total;

    public VSumGoBaView() {
    }

    public String getGroupOwner() {
        return groupOwner;
    }

    public void setGroupOwner(String groupOwner) {
        this.groupOwner = groupOwner;
    }

    public BigInteger getVol() {
        return vol;
    }

    public void setVol(BigInteger vol) {
        this.vol = vol;
    }

    public BigInteger getOtc() {
        return otc;
    }

    public void setOtc(BigInteger otc) {
        this.otc = otc;
    }

    public BigInteger getSewa() {
        return sewa;
    }

    public void setSewa(BigInteger sewa) {
        this.sewa = sewa;
    }

    public BigInteger getTotal() {
        return total;
    }

    public void setTotal(BigInteger total) {
        this.total = total;
    }
    
}
