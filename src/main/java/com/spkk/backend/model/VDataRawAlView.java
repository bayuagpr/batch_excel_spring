/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.spkk.backend.model;

import lombok.ToString;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.lang.String;
import javax.persistence.*;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author bayua
 */
@Entity
@Table(name = "V_DATA_RAW_AL")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "VDataRawAlView.findAll", query = "SELECT v FROM VDataRawAlView v")
    , @NamedQuery(name = "VDataRawAlView.findByAlNo", query = "SELECT v FROM VDataRawAlView v WHERE v.alNo = :alNo")
    , @NamedQuery(name = "VDataRawAlView.findByAlStatus", query = "SELECT v FROM VDataRawAlView v WHERE v.alStatus = :alStatus")
    , @NamedQuery(name = "VDataRawAlView.findByAlType", query = "SELECT v FROM VDataRawAlView v WHERE v.alType = :alType")
    , @NamedQuery(name = "VDataRawAlView.findByPaId", query = "SELECT v FROM VDataRawAlView v WHERE v.paId = :paId")
    , @NamedQuery(name = "VDataRawAlView.findByCustid", query = "SELECT v FROM VDataRawAlView v WHERE v.custid = :custid")
    , @NamedQuery(name = "VDataRawAlView.findByCustname", query = "SELECT v FROM VDataRawAlView v WHERE v.custname = :custname")
    , @NamedQuery(name = "VDataRawAlView.findBySegment", query = "SELECT v FROM VDataRawAlView v WHERE v.segment = :segment")
    , @NamedQuery(name = "VDataRawAlView.findBySid", query = "SELECT v FROM VDataRawAlView v WHERE v.sid = :sid")
    , @NamedQuery(name = "VDataRawAlView.findByAddressOri", query = "SELECT v FROM VDataRawAlView v WHERE v.addressOri = :addressOri")
    , @NamedQuery(name = "VDataRawAlView.findBySbuOri", query = "SELECT v FROM VDataRawAlView v WHERE v.sbuOri = :sbuOri")
    , @NamedQuery(name = "VDataRawAlView.findByAddressTer", query = "SELECT v FROM VDataRawAlView v WHERE v.addressTer = :addressTer")
    , @NamedQuery(name = "VDataRawAlView.findBySbuTer", query = "SELECT v FROM VDataRawAlView v WHERE v.sbuTer = :sbuTer")
    , @NamedQuery(name = "VDataRawAlView.findByStartbilldate", query = "SELECT v FROM VDataRawAlView v WHERE v.startbilldate = :startbilldate")
    , @NamedQuery(name = "VDataRawAlView.findByLastinvoicedate", query = "SELECT v FROM VDataRawAlView v WHERE v.lastinvoicedate = :lastinvoicedate")
    , @NamedQuery(name = "VDataRawAlView.findByDeactivationdate", query = "SELECT v FROM VDataRawAlView v WHERE v.deactivationdate = :deactivationdate")
    , @NamedQuery(name = "VDataRawAlView.findByHoldInvoice", query = "SELECT v FROM VDataRawAlView v WHERE v.holdInvoice = :holdInvoice")
    , @NamedQuery(name = "VDataRawAlView.findByAgrNo", query = "SELECT v FROM VDataRawAlView v WHERE v.agrNo = :agrNo")
    , @NamedQuery(name = "VDataRawAlView.findByLayanan", query = "SELECT v FROM VDataRawAlView v WHERE v.layanan = :layanan")
    , @NamedQuery(name = "VDataRawAlView.findByBandwidth", query = "SELECT v FROM VDataRawAlView v WHERE v.bandwidth = :bandwidth")
    , @NamedQuery(name = "VDataRawAlView.findByQty", query = "SELECT v FROM VDataRawAlView v WHERE v.qty = :qty")
    , @NamedQuery(name = "VDataRawAlView.findByBiayaSewa", query = "SELECT v FROM VDataRawAlView v WHERE v.biayaSewa = :biayaSewa")
    , @NamedQuery(name = "VDataRawAlView.findByBiayaInstalasi", query = "SELECT v FROM VDataRawAlView v WHERE v.biayaInstalasi = :biayaInstalasi")
    , @NamedQuery(name = "VDataRawAlView.findByBiayaRelokasi", query = "SELECT v FROM VDataRawAlView v WHERE v.biayaRelokasi = :biayaRelokasi")
    , @NamedQuery(name = "VDataRawAlView.findByBiayaSewaLama", query = "SELECT v FROM VDataRawAlView v WHERE v.biayaSewaLama = :biayaSewaLama")
    , @NamedQuery(name = "VDataRawAlView.findByAlReference", query = "SELECT v FROM VDataRawAlView v WHERE v.alReference = :alReference")
    , @NamedQuery(name = "VDataRawAlView.findBySbuOwner", query = "SELECT v FROM VDataRawAlView v WHERE v.sbuOwner = :sbuOwner")
    , @NamedQuery(name = "VDataRawAlView.findByOwner", query = "SELECT v FROM VDataRawAlView v WHERE v.owner = :owner")
    , @NamedQuery(name = "VDataRawAlView.findByCreatedon", query = "SELECT v FROM VDataRawAlView v WHERE v.createdon = :createdon")
    , @NamedQuery(name = "VDataRawAlView.findByArNo", query = "SELECT v FROM VDataRawAlView v WHERE v.arNo = :arNo")
    , @NamedQuery(name = "VDataRawAlView.findByArStatus", query = "SELECT v FROM VDataRawAlView v WHERE v.arStatus = :arStatus")
    , @NamedQuery(name = "VDataRawAlView.findByPaStatus", query = "SELECT v FROM VDataRawAlView v WHERE v.paStatus = :paStatus")
    , @NamedQuery(name = "VDataRawAlView.findByTahunTagih", query = "SELECT v FROM VDataRawAlView v WHERE v.tahunTagih = :tahunTagih")
    , @NamedQuery(name = "VDataRawAlView.findByBulanTagih", query = "SELECT v FROM VDataRawAlView v WHERE v.bulanTagih = :bulanTagih")
    , @NamedQuery(name = "VDataRawAlView.findByTahunDeactivated", query = "SELECT v FROM VDataRawAlView v WHERE v.tahunDeactivated = :tahunDeactivated")
    , @NamedQuery(name = "VDataRawAlView.findByBulanDeactivated", query = "SELECT v FROM VDataRawAlView v WHERE v.bulanDeactivated = :bulanDeactivated")
    , @NamedQuery(name = "VDataRawAlView.findByHargaFinal", query = "SELECT v FROM VDataRawAlView v WHERE v.hargaFinal = :hargaFinal")
    , @NamedQuery(name = "VDataRawAlView.findByCoActive", query = "SELECT v FROM VDataRawAlView v WHERE v.coActive = :coActive")
    , @NamedQuery(name = "VDataRawAlView.findByCoDeactive", query = "SELECT v FROM VDataRawAlView v WHERE v.coDeactive = :coDeactive")
    , @NamedQuery(name = "VDataRawAlView.findByCoInactive", query = "SELECT v FROM VDataRawAlView v WHERE v.coInactive = :coInactive")
    , @NamedQuery(name = "VDataRawAlView.findByCarryOver2017", query = "SELECT v FROM VDataRawAlView v WHERE v.carryOver2017 = :carryOver2017")
    , @NamedQuery(name = "VDataRawAlView.findByNrNew", query = "SELECT v FROM VDataRawAlView v WHERE v.nrNew = :nrNew")
    , @NamedQuery(name = "VDataRawAlView.findByNrUpgrade", query = "SELECT v FROM VDataRawAlView v WHERE v.nrUpgrade = :nrUpgrade")
    , @NamedQuery(name = "VDataRawAlView.findByNrDowngrade", query = "SELECT v FROM VDataRawAlView v WHERE v.nrDowngrade = :nrDowngrade")
    , @NamedQuery(name = "VDataRawAlView.findByNrChangeTariff", query = "SELECT v FROM VDataRawAlView v WHERE v.nrChangeTariff = :nrChangeTariff")
    , @NamedQuery(name = "VDataRawAlView.findByNrRelocation", query = "SELECT v FROM VDataRawAlView v WHERE v.nrRelocation = :nrRelocation")
    , @NamedQuery(name = "VDataRawAlView.findByNrOtc", query = "SELECT v FROM VDataRawAlView v WHERE v.nrOtc = :nrOtc")
    , @NamedQuery(name = "VDataRawAlView.findByNewRevenue", query = "SELECT v FROM VDataRawAlView v WHERE v.newRevenue = :newRevenue")
    , @NamedQuery(name = "VDataRawAlView.findByGroupOwner", query = "SELECT v FROM VDataRawAlView v WHERE v.groupOwner = :groupOwner")
    , @NamedQuery(name = "VDataRawAlView.findByCoJuni", query = "SELECT v FROM VDataRawAlView v WHERE v.coJuni = :coJuni")
    , @NamedQuery(name = "VDataRawAlView.findByNrJuni", query = "SELECT v FROM VDataRawAlView v WHERE v.nrJuni = :nrJuni")
    , @NamedQuery(name = "VDataRawAlView.findByCo2019", query = "SELECT v FROM VDataRawAlView v WHERE v.co2019 = :co2019")})
@ToString(includeFieldNames=true)
public class VDataRawAlView implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Size(max = 400)
    @Column(name = "AL_NO")
    private String alNo;
    @Size(max = 120)
    @Column(name = "AL_STATUS")
    private String alStatus;
    @Size(max = 120)
    @Column(name = "AL_TYPE")
    private String alType;
    @Size(max = 400)
    @Column(name = "PA_ID")
    private String paId;
    @Size(max = 400)
    @Column(name = "CUSTID")
    private String custid;
    @Size(max = 1200)
    @Column(name = "CUSTNAME")
    private String custname;
    @Size(max = 400)
    @Column(name = "SEGMENT")
    private String segment;
    @Size(max = 400)
    @Column(name = "SID")
    private String sid;
    @Size(max = 1200)
    @Column(name = "ADDRESS_ORI")
    private String addressOri;
    @Size(max = 400)
    @Column(name = "SBU_ORI")
    private String sbuOri;
    @Size(max = 1200)
    @Column(name = "ADDRESS_TER")
    private String addressTer;
    @Size(max = 400)
    @Column(name = "SBU_TER")
    private String sbuTer;
    @Column(name = "STARTBILLDATE")
    private String startbilldate;
    @Column(name = "LASTINVOICEDATE")
    private String lastinvoicedate;
    @Column(name = "DEACTIVATIONDATE")
    private String deactivationdate;
    @Size(max = 80)
    @Column(name = "HOLD_INVOICE")
    private String holdInvoice;
    @Size(max = 400)
    @Column(name = "AGR_NO")
    private String agrNo;
    @Size(max = 400)
    @Column(name = "LAYANAN")
    private String layanan;
    @Size(max = 80)
    @Column(name = "BANDWIDTH")
    private String bandwidth;
    @Column(name = "QTY")
    private BigInteger qty;
    @Column(name = "BIAYA_SEWA")
    private BigDecimal biayaSewa;
    @Column(name = "BIAYA_INSTALASI")
    private BigDecimal biayaInstalasi;
    @Column(name = "BIAYA_RELOKASI")
    private BigDecimal biayaRelokasi;
    @Column(name = "BIAYA_SEWA_LAMA")
    private BigDecimal biayaSewaLama;
    @Size(max = 400)
    @Column(name = "AL_REFERENCE")
    private String alReference;
    @Size(max = 400)
    @Column(name = "SBU_OWNER")
    private String sbuOwner;
    @Size(max = 200)
    @Column(name = "OWNER")
    private String owner;
    @Column(name = "CREATEDON")
    private String createdon;
    @Size(max = 100)
    @Column(name = "AR_NO")
    private String arNo;
    @Size(max = 30)
    @Column(name = "AR_STATUS")
    private String arStatus;
    @Size(max = 30)
    @Column(name = "PA_STATUS")
    private String paStatus;
    @Size(max = 50)
    @Column(name = "TAHUN_TAGIH")
    private String tahunTagih;
    @Size(max = 50)
    @Column(name = "BULAN_TAGIH")
    private String bulanTagih;
    @Size(max = 50)
    @Column(name = "TAHUN_DEACTIVATED")
    private String tahunDeactivated;
    @Size(max = 50)
    @Column(name = "BULAN_DEACTIVATED")
    private String bulanDeactivated;
    @Column(name = "HARGA_FINAL")
    private BigDecimal hargaFinal;
    @Column(name = "CO_ACTIVE")
    private BigDecimal coActive;
    @Column(name = "CO_DEACTIVE")
    private BigDecimal coDeactive;
    @Column(name = "CO_INACTIVE")
    private BigDecimal coInactive;
    @Column(name = "CARRY_OVER_2017")
    private BigDecimal carryOver2017;
    @Column(name = "NR_NEW")
    private BigDecimal nrNew;
    @Column(name = "NR_UPGRADE")
    private BigDecimal nrUpgrade;
    @Column(name = "NR_DOWNGRADE")
    private BigDecimal nrDowngrade;
    @Column(name = "NR_CHANGE_TARIFF")
    private BigDecimal nrChangeTariff;
    @Column(name = "NR_RELOCATION")
    private BigDecimal nrRelocation;
    @Column(name = "NR_OTC")
    private BigDecimal nrOtc;
    @Column(name = "NEW_REVENUE")
    private BigDecimal newRevenue;
    @Size(max = 50)
    @Column(name = "GROUP_OWNER")
    private String groupOwner;
    @Column(name = "CO_JUNI")
    private BigDecimal coJuni;
    @Column(name = "NR_JUNI")
    private BigDecimal nrJuni;
    @Column(name = "CO_2019")
    private BigDecimal co2019;

    public VDataRawAlView() {
    }

    public String getAlNo() {
        return alNo;
    }

    public void setAlNo(String alNo) {
        this.alNo = alNo;
    }

    public String getAlStatus() {
        return alStatus;
    }

    public void setAlStatus(String alStatus) {
        this.alStatus = alStatus;
    }

    public String getAlType() {
        return alType;
    }

    public void setAlType(String alType) {
        this.alType = alType;
    }

    public String getPaId() {
        return paId;
    }

    public void setPaId(String paId) {
        this.paId = paId;
    }

    public String getCustid() {
        return custid;
    }

    public void setCustid(String custid) {
        this.custid = custid;
    }

    public String getCustname() {
        return custname;
    }

    public void setCustname(String custname) {
        this.custname = custname;
    }

    public String getSegment() {
        return segment;
    }

    public void setSegment(String segment) {
        this.segment = segment;
    }

    public String getSid() {
        return sid;
    }

    public void setSid(String sid) {
        this.sid = sid;
    }

    public String getAddressOri() {
        return addressOri;
    }

    public void setAddressOri(String addressOri) {
        this.addressOri = addressOri;
    }

    public String getSbuOri() {
        return sbuOri;
    }

    public void setSbuOri(String sbuOri) {
        this.sbuOri = sbuOri;
    }

    public String getAddressTer() {
        return addressTer;
    }

    public void setAddressTer(String addressTer) {
        this.addressTer = addressTer;
    }

    public String getSbuTer() {
        return sbuTer;
    }

    public void setSbuTer(String sbuTer) {
        this.sbuTer = sbuTer;
    }

    public String getStartbilldate() {
        return startbilldate;
    }

    public void setStartbilldate(String startbilldate) {
        this.startbilldate = startbilldate;
    }

    public String getLastinvoicedate() {
        return lastinvoicedate;
    }

    public void setLastinvoicedate(String lastinvoicedate) {
        this.lastinvoicedate = lastinvoicedate;
    }

    public String getDeactivationdate() {
        return deactivationdate;
    }

    public void setDeactivationdate(String deactivationdate) {
        this.deactivationdate = deactivationdate;
    }

    public String getHoldInvoice() {
        return holdInvoice;
    }

    public void setHoldInvoice(String holdInvoice) {
        this.holdInvoice = holdInvoice;
    }

    public String getAgrNo() {
        return agrNo;
    }

    public void setAgrNo(String agrNo) {
        this.agrNo = agrNo;
    }

    public String getLayanan() {
        return layanan;
    }

    public void setLayanan(String layanan) {
        this.layanan = layanan;
    }

    public String getBandwidth() {
        return bandwidth;
    }

    public void setBandwidth(String bandwidth) {
        this.bandwidth = bandwidth;
    }

    public BigInteger getQty() {
        return qty;
    }

    public void setQty(BigInteger qty) {
        this.qty = qty;
    }

    public BigDecimal getBiayaSewa() {
        return biayaSewa;
    }

    public void setBiayaSewa(BigDecimal biayaSewa) {
        this.biayaSewa = biayaSewa;
    }

    public BigDecimal getBiayaInstalasi() {
        return biayaInstalasi;
    }

    public void setBiayaInstalasi(BigDecimal biayaInstalasi) {
        this.biayaInstalasi = biayaInstalasi;
    }

    public BigDecimal getBiayaRelokasi() {
        return biayaRelokasi;
    }

    public void setBiayaRelokasi(BigDecimal biayaRelokasi) {
        this.biayaRelokasi = biayaRelokasi;
    }

    public BigDecimal getBiayaSewaLama() {
        return biayaSewaLama;
    }

    public void setBiayaSewaLama(BigDecimal biayaSewaLama) {
        this.biayaSewaLama = biayaSewaLama;
    }

    public String getAlReference() {
        return alReference;
    }

    public void setAlReference(String alReference) {
        this.alReference = alReference;
    }

    public String getSbuOwner() {
        return sbuOwner;
    }

    public void setSbuOwner(String sbuOwner) {
        this.sbuOwner = sbuOwner;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getCreatedon() {
        return createdon;
    }

    public void setCreatedon(String createdon) {
        this.createdon = createdon;
    }

    public String getArNo() {
        return arNo;
    }

    public void setArNo(String arNo) {
        this.arNo = arNo;
    }

    public String getArStatus() {
        return arStatus;
    }

    public void setArStatus(String arStatus) {
        this.arStatus = arStatus;
    }

    public String getPaStatus() {
        return paStatus;
    }

    public void setPaStatus(String paStatus) {
        this.paStatus = paStatus;
    }

    public String getTahunTagih() {
        return tahunTagih;
    }

    public void setTahunTagih(String tahunTagih) {
        this.tahunTagih = tahunTagih;
    }

    public String getBulanTagih() {
        return bulanTagih;
    }

    public void setBulanTagih(String bulanTagih) {
        this.bulanTagih = bulanTagih;
    }

    public String getTahunDeactivated() {
        return tahunDeactivated;
    }

    public void setTahunDeactivated(String tahunDeactivated) {
        this.tahunDeactivated = tahunDeactivated;
    }

    public String getBulanDeactivated() {
        return bulanDeactivated;
    }

    public void setBulanDeactivated(String bulanDeactivated) {
        this.bulanDeactivated = bulanDeactivated;
    }

    public BigDecimal getHargaFinal() {
        return hargaFinal;
    }

    public void setHargaFinal(BigDecimal hargaFinal) {
        this.hargaFinal = hargaFinal;
    }

    public BigDecimal getCoActive() {
        return coActive;
    }

    public void setCoActive(BigDecimal coActive) {
        this.coActive = coActive;
    }

    public BigDecimal getCoDeactive() {
        return coDeactive;
    }

    public void setCoDeactive(BigDecimal coDeactive) {
        this.coDeactive = coDeactive;
    }

    public BigDecimal getCoInactive() {
        return coInactive;
    }

    public void setCoInactive(BigDecimal coInactive) {
        this.coInactive = coInactive;
    }

    public BigDecimal getCarryOver2017() {
        return carryOver2017;
    }

    public void setCarryOver2017(BigDecimal carryOver2017) {
        this.carryOver2017 = carryOver2017;
    }

    public BigDecimal getNrNew() {
        return nrNew;
    }

    public void setNrNew(BigDecimal nrNew) {
        this.nrNew = nrNew;
    }

    public BigDecimal getNrUpgrade() {
        return nrUpgrade;
    }

    public void setNrUpgrade(BigDecimal nrUpgrade) {
        this.nrUpgrade = nrUpgrade;
    }

    public BigDecimal getNrDowngrade() {
        return nrDowngrade;
    }

    public void setNrDowngrade(BigDecimal nrDowngrade) {
        this.nrDowngrade = nrDowngrade;
    }

    public BigDecimal getNrChangeTariff() {
        return nrChangeTariff;
    }

    public void setNrChangeTariff(BigDecimal nrChangeTariff) {
        this.nrChangeTariff = nrChangeTariff;
    }

    public BigDecimal getNrRelocation() {
        return nrRelocation;
    }

    public void setNrRelocation(BigDecimal nrRelocation) {
        this.nrRelocation = nrRelocation;
    }

    public BigDecimal getNrOtc() {
        return nrOtc;
    }

    public void setNrOtc(BigDecimal nrOtc) {
        this.nrOtc = nrOtc;
    }

    public BigDecimal getNewRevenue() {
        return newRevenue;
    }

    public void setNewRevenue(BigDecimal newRevenue) {
        this.newRevenue = newRevenue;
    }

    public String getGroupOwner() {
        return groupOwner;
    }

    public void setGroupOwner(String groupOwner) {
        this.groupOwner = groupOwner;
    }

    public BigDecimal getCoJuni() {
        return coJuni;
    }

    public void setCoJuni(BigDecimal coJuni) {
        this.coJuni = coJuni;
    }

    public BigDecimal getNrJuni() {
        return nrJuni;
    }

    public void setNrJuni(BigDecimal nrJuni) {
        this.nrJuni = nrJuni;
    }

    public BigDecimal getCo2019() {
        return co2019;
    }

    public void setCo2019(BigDecimal co2019) {
        this.co2019 = co2019;
    }
    
}
