/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.spkk.backend.model;

import lombok.ToString;

import java.io.Serializable;
import java.math.BigInteger;
import javax.persistence.*;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author bayua
 */
@Entity
@Table(name = "V_AL_EST_DEC_FIX")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "VAlEstDecFixView.findAll", query = "SELECT v FROM VAlEstDecFixView v")
    , @NamedQuery(name = "VAlEstDecFixView.findByGroupOwner", query = "SELECT v FROM VAlEstDecFixView v WHERE v.groupOwner = :groupOwner")
    , @NamedQuery(name = "VAlEstDecFixView.findByTarget", query = "SELECT v FROM VAlEstDecFixView v WHERE v.target = :target")
    , @NamedQuery(name = "VAlEstDecFixView.findByCoTahunSebelumnya", query = "SELECT v FROM VAlEstDecFixView v WHERE v.coTahunSebelumnya = :coTahunSebelumnya")
    , @NamedQuery(name = "VAlEstDecFixView.findByNrTahunIni", query = "SELECT v FROM VAlEstDecFixView v WHERE v.nrTahunIni = :nrTahunIni")
    , @NamedQuery(name = "VAlEstDecFixView.findByTotal", query = "SELECT v FROM VAlEstDecFixView v WHERE v.total = :total")
    , @NamedQuery(name = "VAlEstDecFixView.findByRealisasiTerhadapTarget", query = "SELECT v FROM VAlEstDecFixView v WHERE v.realisasiTerhadapTarget = :realisasiTerhadapTarget")})
@ToString(includeFieldNames=true)
public class VAlEstDecFixView implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Size(max = 50)
    @Column(name = "GROUP_OWNER")
    private String groupOwner;
    @Column(name = "TARGET")
    private BigInteger target;
    @Column(name = "CO_TAHUN_SEBELUMNYA")
    private BigInteger coTahunSebelumnya;
    @Column(name = "NR_TAHUN_INI")
    private BigInteger nrTahunIni;
    @Column(name = "TOTAL")
    private BigInteger total;
    @Size(max = 22)
    @Column(name = "REALISASI_TERHADAP_TARGET")
    private String realisasiTerhadapTarget;

    public VAlEstDecFixView() {
    }

    public String getGroupOwner() {
        return groupOwner;
    }

    public void setGroupOwner(String groupOwner) {
        this.groupOwner = groupOwner;
    }

    public BigInteger getTarget() {
        return target;
    }

    public void setTarget(BigInteger target) {
        this.target = target;
    }

    public BigInteger getCoTahunSebelumnya() {
        return coTahunSebelumnya;
    }

    public void setCoTahunSebelumnya(BigInteger coTahunSebelumnya) {
        this.coTahunSebelumnya = coTahunSebelumnya;
    }

    public BigInteger getNrTahunIni() {
        return nrTahunIni;
    }

    public void setNrTahunIni(BigInteger nrTahunIni) {
        this.nrTahunIni = nrTahunIni;
    }

    public BigInteger getTotal() {
        return total;
    }

    public void setTotal(BigInteger total) {
        this.total = total;
    }

    public String getRealisasiTerhadapTarget() {
        return realisasiTerhadapTarget;
    }

    public void setRealisasiTerhadapTarget(String realisasiTerhadapTarget) {
        this.realisasiTerhadapTarget = realisasiTerhadapTarget;
    }
    
}
