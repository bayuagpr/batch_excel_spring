/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.spkk.backend.model;

import lombok.ToString;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author bayua
 */
@Entity
@Table(name = "GROUP_OWNER")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "GroupOwnerEntity.findAll", query = "SELECT g FROM GroupOwnerEntity g")
    , @NamedQuery(name = "GroupOwnerEntity.findBySegment", query = "SELECT g FROM GroupOwnerEntity g WHERE g.segment = :segment")
    , @NamedQuery(name = "GroupOwnerEntity.findBySbuOwner", query = "SELECT g FROM GroupOwnerEntity g WHERE g.sbuOwner = :sbuOwner")
    , @NamedQuery(name = "GroupOwnerEntity.findByGroupOwner", query = "SELECT g FROM GroupOwnerEntity g WHERE g.groupOwner = :groupOwner")
    , @NamedQuery(name = "GroupOwnerEntity.findById", query = "SELECT g FROM GroupOwnerEntity g WHERE g.groupOwnerId = :groupOwnerId")})
@ToString(includeFieldNames=true)
public class GroupOwnerEntity implements Serializable {

    private static final long serialVersionUID = 1L;
    @Size(max = 100)
    @Column(name = "SEGMENT")
    private String segment;
    @Size(max = 100)
    @Column(name = "SBU_OWNER")
    private String sbuOwner;
    @Size(max = 100)
    @Column(name = "GROUP_OWNER")
    private String groupOwner;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "GROUP_OWNER_ID")
    private BigDecimal groupOwnerId;

    public GroupOwnerEntity() {
    }

    public GroupOwnerEntity(BigDecimal groupOwnerId) {
        this.groupOwnerId = groupOwnerId;
    }

    public String getSegment() {
        return segment;
    }

    public void setSegment(String segment) {
        this.segment = segment;
    }

    public String getSbuOwner() {
        return sbuOwner;
    }

    public void setSbuOwner(String sbuOwner) {
        this.sbuOwner = sbuOwner;
    }

    public String getGroupOwner() {
        return groupOwner;
    }

    public void setGroupOwner(String groupOwner) {
        this.groupOwner = groupOwner;
    }

    public BigDecimal getGroupOwnerId() {
        return groupOwnerId;
    }

    public void setGroupOwnerId(BigDecimal id) {
        this.groupOwnerId = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (groupOwnerId != null ? groupOwnerId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof GroupOwnerEntity)) {
            return false;
        }
        GroupOwnerEntity other = (GroupOwnerEntity) object;
        if ((this.groupOwnerId == null && other.groupOwnerId != null) || (this.groupOwnerId != null && !this.groupOwnerId.equals(other.groupOwnerId))) {
            return false;
        }
        return true;
    }


}
