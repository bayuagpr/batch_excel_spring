/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.spkk.backend.model;

import lombok.ToString;

import java.io.Serializable;
import java.math.BigInteger;
import javax.persistence.*;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author bayua
 */
@Entity
@Table(name = "V_AL_EST_JUNI_FIX")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "VAlEstJuniFixView.findAll", query = "SELECT v FROM VAlEstJuniFixView v")
    , @NamedQuery(name = "VAlEstJuniFixView.findByGroupOwner", query = "SELECT v FROM VAlEstJuniFixView v WHERE v.groupOwner = :groupOwner")
    , @NamedQuery(name = "VAlEstJuniFixView.findByTargetSemester", query = "SELECT v FROM VAlEstJuniFixView v WHERE v.targetSemester = :targetSemester")
    , @NamedQuery(name = "VAlEstJuniFixView.findByCoTahunSebelumnya", query = "SELECT v FROM VAlEstJuniFixView v WHERE v.coTahunSebelumnya = :coTahunSebelumnya")
    , @NamedQuery(name = "VAlEstJuniFixView.findByNrTahunIni", query = "SELECT v FROM VAlEstJuniFixView v WHERE v.nrTahunIni = :nrTahunIni")
    , @NamedQuery(name = "VAlEstJuniFixView.findByTotal", query = "SELECT v FROM VAlEstJuniFixView v WHERE v.total = :total")
    , @NamedQuery(name = "VAlEstJuniFixView.findByRealisasiTerhadapTarget", query = "SELECT v FROM VAlEstJuniFixView v WHERE v.realisasiTerhadapTarget = :realisasiTerhadapTarget")})
@ToString(includeFieldNames=true)
public class VAlEstJuniFixView implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Size(max = 50)
    @Column(name = "GROUP_OWNER")
    private String groupOwner;
    @Column(name = "TARGET_SEMESTER")
    private BigInteger targetSemester;
    @Column(name = "CO_TAHUN_SEBELUMNYA")
    private BigInteger coTahunSebelumnya;
    @Column(name = "NR_TAHUN_INI")
    private BigInteger nrTahunIni;
    @Column(name = "TOTAL")
    private BigInteger total;
    @Size(max = 22)
    @Column(name = "REALISASI_TERHADAP_TARGET")
    private String realisasiTerhadapTarget;

    public VAlEstJuniFixView() {
    }

    public String getGroupOwner() {
        return groupOwner;
    }

    public void setGroupOwner(String groupOwner) {
        this.groupOwner = groupOwner;
    }

    public BigInteger getTargetSemester() {
        return targetSemester;
    }

    public void setTargetSemester(BigInteger targetSemester) {
        this.targetSemester = targetSemester;
    }

    public BigInteger getCoTahunSebelumnya() {
        return coTahunSebelumnya;
    }

    public void setCoTahunSebelumnya(BigInteger coTahunSebelumnya) {
        this.coTahunSebelumnya = coTahunSebelumnya;
    }

    public BigInteger getNrTahunIni() {
        return nrTahunIni;
    }

    public void setNrTahunIni(BigInteger nrTahunIni) {
        this.nrTahunIni = nrTahunIni;
    }

    public BigInteger getTotal() {
        return total;
    }

    public void setTotal(BigInteger total) {
        this.total = total;
    }

    public String getRealisasiTerhadapTarget() {
        return realisasiTerhadapTarget;
    }

    public void setRealisasiTerhadapTarget(String realisasiTerhadapTarget) {
        this.realisasiTerhadapTarget = realisasiTerhadapTarget;
    }
    
}
