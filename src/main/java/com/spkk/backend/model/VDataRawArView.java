/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.spkk.backend.model;

import lombok.ToString;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;
import javax.persistence.*;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author bayua
 */
@Entity
@Table(name = "V_DATA_RAW_AR")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "VDataRawArView.findAll", query = "SELECT v FROM VDataRawArView v")
    , @NamedQuery(name = "VDataRawArView.findByArNo", query = "SELECT v FROM VDataRawArView v WHERE v.arNo = :arNo")
    , @NamedQuery(name = "VDataRawArView.findByArType", query = "SELECT v FROM VDataRawArView v WHERE v.arType = :arType")
    , @NamedQuery(name = "VDataRawArView.findByArStatus", query = "SELECT v FROM VDataRawArView v WHERE v.arStatus = :arStatus")
    , @NamedQuery(name = "VDataRawArView.findByArDate", query = "SELECT v FROM VDataRawArView v WHERE v.arDate = :arDate")
    , @NamedQuery(name = "VDataRawArView.findByCustName", query = "SELECT v FROM VDataRawArView v WHERE v.custName = :custName")
    , @NamedQuery(name = "VDataRawArView.findBySegment", query = "SELECT v FROM VDataRawArView v WHERE v.segment = :segment")
    , @NamedQuery(name = "VDataRawArView.findByBaDate", query = "SELECT v FROM VDataRawArView v WHERE v.baDate = :baDate")
    , @NamedQuery(name = "VDataRawArView.findByCancelDate", query = "SELECT v FROM VDataRawArView v WHERE v.cancelDate = :cancelDate")
    , @NamedQuery(name = "VDataRawArView.findByApprovalDate", query = "SELECT v FROM VDataRawArView v WHERE v.approvalDate = :approvalDate")
    , @NamedQuery(name = "VDataRawArView.findByTargetdate", query = "SELECT v FROM VDataRawArView v WHERE v.targetdate = :targetdate")
    , @NamedQuery(name = "VDataRawArView.findByClosedate", query = "SELECT v FROM VDataRawArView v WHERE v.closedate = :closedate")
    , @NamedQuery(name = "VDataRawArView.findByDeactivationdate", query = "SELECT v FROM VDataRawArView v WHERE v.deactivationdate = :deactivationdate")
    , @NamedQuery(name = "VDataRawArView.findBySid", query = "SELECT v FROM VDataRawArView v WHERE v.sid = :sid")
    , @NamedQuery(name = "VDataRawArView.findByAddressOriginating", query = "SELECT v FROM VDataRawArView v WHERE v.addressOriginating = :addressOriginating")
    , @NamedQuery(name = "VDataRawArView.findBySbuOri", query = "SELECT v FROM VDataRawArView v WHERE v.sbuOri = :sbuOri")
    , @NamedQuery(name = "VDataRawArView.findByAddressTerminating", query = "SELECT v FROM VDataRawArView v WHERE v.addressTerminating = :addressTerminating")
    , @NamedQuery(name = "VDataRawArView.findBySbuTer", query = "SELECT v FROM VDataRawArView v WHERE v.sbuTer = :sbuTer")
    , @NamedQuery(name = "VDataRawArView.findByAgrId", query = "SELECT v FROM VDataRawArView v WHERE v.agrId = :agrId")
    , @NamedQuery(name = "VDataRawArView.findByLayanan", query = "SELECT v FROM VDataRawArView v WHERE v.layanan = :layanan")
    , @NamedQuery(name = "VDataRawArView.findByBandwidth", query = "SELECT v FROM VDataRawArView v WHERE v.bandwidth = :bandwidth")
    , @NamedQuery(name = "VDataRawArView.findByQty", query = "SELECT v FROM VDataRawArView v WHERE v.qty = :qty")
    , @NamedQuery(name = "VDataRawArView.findByBiayaSewa", query = "SELECT v FROM VDataRawArView v WHERE v.biayaSewa = :biayaSewa")
    , @NamedQuery(name = "VDataRawArView.findByBiayaInstalasi", query = "SELECT v FROM VDataRawArView v WHERE v.biayaInstalasi = :biayaInstalasi")
    , @NamedQuery(name = "VDataRawArView.findByBiayaRelokasi", query = "SELECT v FROM VDataRawArView v WHERE v.biayaRelokasi = :biayaRelokasi")
    , @NamedQuery(name = "VDataRawArView.findByActivationactualdate", query = "SELECT v FROM VDataRawArView v WHERE v.activationactualdate = :activationactualdate")
    , @NamedQuery(name = "VDataRawArView.findBySbuOwner", query = "SELECT v FROM VDataRawArView v WHERE v.sbuOwner = :sbuOwner")
    , @NamedQuery(name = "VDataRawArView.findByOwner", query = "SELECT v FROM VDataRawArView v WHERE v.owner = :owner")
    , @NamedQuery(name = "VDataRawArView.findByPaId", query = "SELECT v FROM VDataRawArView v WHERE v.paId = :paId")
    , @NamedQuery(name = "VDataRawArView.findByPaStatus", query = "SELECT v FROM VDataRawArView v WHERE v.paStatus = :paStatus")
    , @NamedQuery(name = "VDataRawArView.findByPsId", query = "SELECT v FROM VDataRawArView v WHERE v.psId = :psId")
    , @NamedQuery(name = "VDataRawArView.findByPriceLama", query = "SELECT v FROM VDataRawArView v WHERE v.priceLama = :priceLama")
    , @NamedQuery(name = "VDataRawArView.findBySidLama", query = "SELECT v FROM VDataRawArView v WHERE v.sidLama = :sidLama")
    , @NamedQuery(name = "VDataRawArView.findByCreatedon", query = "SELECT v FROM VDataRawArView v WHERE v.createdon = :createdon")
    , @NamedQuery(name = "VDataRawArView.findByAlReferenceNo", query = "SELECT v FROM VDataRawArView v WHERE v.alReferenceNo = :alReferenceNo")
    , @NamedQuery(name = "VDataRawArView.findByAlNo", query = "SELECT v FROM VDataRawArView v WHERE v.alNo = :alNo")
    , @NamedQuery(name = "VDataRawArView.findByAlStatus", query = "SELECT v FROM VDataRawArView v WHERE v.alStatus = :alStatus")
    , @NamedQuery(name = "VDataRawArView.findByBaaId", query = "SELECT v FROM VDataRawArView v WHERE v.baaId = :baaId")
    , @NamedQuery(name = "VDataRawArView.findByHargaFinal", query = "SELECT v FROM VDataRawArView v WHERE v.hargaFinal = :hargaFinal")
    , @NamedQuery(name = "VDataRawArView.findByTahunBa", query = "SELECT v FROM VDataRawArView v WHERE v.tahunBa = :tahunBa")
    , @NamedQuery(name = "VDataRawArView.findByTargetSo", query = "SELECT v FROM VDataRawArView v WHERE v.targetSo = :targetSo")
    , @NamedQuery(name = "VDataRawArView.findByAdaAl", query = "SELECT v FROM VDataRawArView v WHERE v.adaAl = :adaAl")
    , @NamedQuery(name = "VDataRawArView.findByBa", query = "SELECT v FROM VDataRawArView v WHERE v.ba = :ba")
    , @NamedQuery(name = "VDataRawArView.findByPotensiBa", query = "SELECT v FROM VDataRawArView v WHERE v.potensiBa = :potensiBa")
    , @NamedQuery(name = "VDataRawArView.findByOtc1", query = "SELECT v FROM VDataRawArView v WHERE v.otc1 = :otc1")
    , @NamedQuery(name = "VDataRawArView.findByJumlahBa", query = "SELECT v FROM VDataRawArView v WHERE v.jumlahBa = :jumlahBa")
    , @NamedQuery(name = "VDataRawArView.findByPotensiProgress", query = "SELECT v FROM VDataRawArView v WHERE v.potensiProgress = :potensiProgress")
    , @NamedQuery(name = "VDataRawArView.findByOtc2", query = "SELECT v FROM VDataRawArView v WHERE v.otc2 = :otc2")
    , @NamedQuery(name = "VDataRawArView.findByJumlahSo", query = "SELECT v FROM VDataRawArView v WHERE v.jumlahSo = :jumlahSo")
    , @NamedQuery(name = "VDataRawArView.findByGroupOwner", query = "SELECT v FROM VDataRawArView v WHERE v.groupOwner = :groupOwner")
    , @NamedQuery(name = "VDataRawArView.findByGroupLayanan", query = "SELECT v FROM VDataRawArView v WHERE v.groupLayanan = :groupLayanan")
    , @NamedQuery(name = "VDataRawArView.findByBa2019", query = "SELECT v FROM VDataRawArView v WHERE v.ba2019 = :ba2019")
    , @NamedQuery(name = "VDataRawArView.findBySo2019", query = "SELECT v FROM VDataRawArView v WHERE v.so2019 = :so2019")
        , @NamedQuery(name = "VDataRawArView.findByCo2019", query = "SELECT v FROM VDataRawArView v WHERE v.co2019 = :co2019")
    , @NamedQuery(name = "VDataRawArView.findByAging", query = "SELECT v FROM VDataRawArView v WHERE v.aging = :aging")})
@ToString(includeFieldNames=true)
public class VDataRawArView implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Size(max = 400)
    @Column(name = "AR_NO")
    private String arNo;
    @Size(max = 120)
    @Column(name = "AR_TYPE")
    private String arType;
    @Size(max = 120)
    @Column(name = "AR_STATUS")
    private String arStatus;
    @Column(name = "AR_DATE")
    private String arDate;
    @Size(max = 1200)
    @Column(name = "CUST_NAME")
    private String custName;
    @Size(max = 400)
    @Column(name = "SEGMENT")
    private String segment;
    @Column(name = "BA_DATE")
    private String baDate;
    @Column(name = "CANCEL_DATE")
    private String cancelDate;
    @Column(name = "APPROVAL_DATE")
    private String approvalDate;
    @Column(name = "TARGETDATE")
    private String targetdate;
    @Column(name = "CLOSEDATE")
    private String closedate;
    @Column(name = "DEACTIVATIONDATE")
    private String deactivationdate;
    @Size(max = 400)
    @Column(name = "SID")
    private String sid;
    @Size(max = 1400)
    @Column(name = "ADDRESS_ORIGINATING")
    private String addressOriginating;
    @Size(max = 400)
    @Column(name = "SBU_ORI")
    private String sbuOri;
    @Size(max = 350)
    @Column(name = "ADDRESS_TERMINATING")
    private String addressTerminating;
    @Size(max = 100)
    @Column(name = "SBU_TER")
    private String sbuTer;
    @Size(max = 400)
    @Column(name = "AGR_ID")
    private String agrId;
    @Size(max = 400)
    @Column(name = "LAYANAN")
    private String layanan;
    @Size(max = 80)
    @Column(name = "BANDWIDTH")
    private String bandwidth;
    @Column(name = "QTY")
    private BigInteger qty;
    @Column(name = "BIAYA_SEWA")
    private BigDecimal biayaSewa;
    @Column(name = "BIAYA_INSTALASI")
    private BigDecimal biayaInstalasi;
    @Column(name = "BIAYA_RELOKASI")
    private BigDecimal biayaRelokasi;
    @Column(name = "ACTIVATIONACTUALDATE")
    private String activationactualdate;
    @Size(max = 400)
    @Column(name = "SBU_OWNER")
    private String sbuOwner;
    @Size(max = 200)
    @Column(name = "OWNER")
    private String owner;
    @Size(max = 400)
    @Column(name = "PA_ID")
    private String paId;
    @Size(max = 120)
    @Column(name = "PA_STATUS")
    private String paStatus;
    @Size(max = 400)
    @Column(name = "PS_ID")
    private String psId;
    @Column(name = "PRICE_LAMA")
    private BigDecimal priceLama;
    @Size(max = 400)
    @Column(name = "SID_LAMA")
    private String sidLama;
    @Column(name = "CREATEDON")
    private String createdon;
    @Size(max = 100)
    @Column(name = "AL_REFERENCE_NO")
    private String alReferenceNo;
    @Size(max = 100)
    @Column(name = "AL_NO")
    private String alNo;
    @Size(max = 30)
    @Column(name = "AL_STATUS")
    private String alStatus;
    @Size(max = 100)
    @Column(name = "BAA_ID")
    private String baaId;
    @Column(name = "HARGA_FINAL")
    private BigDecimal hargaFinal;
    @Size(max = 40)
    @Column(name = "TAHUN_BA")
    private String tahunBa;
    @Size(max = 40)
    @Column(name = "TARGET_SO")
    private String targetSo;
    @Size(max = 40)
    @Column(name = "ADA_AL")
    private String adaAl;
    @Size(max = 40)
    @Column(name = "BA_")
    private String ba;
    @Column(name = "POTENSI_BA")
    private BigDecimal potensiBa;
    @Column(name = "OTC1")
    private BigDecimal otc1;
    @Column(name = "JUMLAH_BA")
    private BigDecimal jumlahBa;
    @Column(name = "POTENSI_PROGRESS")
    private BigDecimal potensiProgress;
    @Column(name = "OTC2")
    private BigDecimal otc2;
    @Column(name = "JUMLAH_SO")
    private BigDecimal jumlahSo;
    @Size(max = 400)
    @Column(name = "GROUP_OWNER")
    private String groupOwner;
    @Size(max = 400)
    @Column(name = "GROUP_LAYANAN")
    private String groupLayanan;
    @Column(name = "BA_2019")
    private BigDecimal ba2019;
    @Column(name = "SO_2019")
    private BigDecimal so2019;
    @Column(name = "CO_2019")
    private BigDecimal co2019;
    @Column(name = "AGING_")
    private BigDecimal aging;

    public VDataRawArView() {
    }

    public String getArNo() {
        return arNo;
    }

    public void setArNo(String arNo) {
        this.arNo = arNo;
    }

    public String getArType() {
        return arType;
    }

    public void setArType(String arType) {
        this.arType = arType;
    }

    public String getArStatus() {
        return arStatus;
    }

    public void setArStatus(String arStatus) {
        this.arStatus = arStatus;
    }

    public String getArDate() {
        return arDate;
    }

    public void setArDate(String arDate) {
        this.arDate = arDate;
    }

    public String getCustName() {
        return custName;
    }

    public void setCustName(String custName) {
        this.custName = custName;
    }

    public String getSegment() {
        return segment;
    }

    public void setSegment(String segment) {
        this.segment = segment;
    }

    public String getBaDate() {
        return baDate;
    }

    public void setBaDate(String baDate) {
        this.baDate = baDate;
    }

    public String getCancelDate() {
        return cancelDate;
    }

    public void setCancelDate(String cancelDate) {
        this.cancelDate = cancelDate;
    }

    public String getApprovalDate() {
        return approvalDate;
    }

    public void setApprovalDate(String approvalDate) {
        this.approvalDate = approvalDate;
    }

    public String getTargetdate() {
        return targetdate;
    }

    public void setTargetdate(String targetdate) {
        this.targetdate = targetdate;
    }

    public String getClosedate() {
        return closedate;
    }

    public void setClosedate(String closedate) {
        this.closedate = closedate;
    }

    public String getDeactivationdate() {
        return deactivationdate;
    }

    public void setDeactivationdate(String deactivationdate) {
        this.deactivationdate = deactivationdate;
    }

    public String getSid() {
        return sid;
    }

    public void setSid(String sid) {
        this.sid = sid;
    }

    public String getAddressOriginating() {
        return addressOriginating;
    }

    public void setAddressOriginating(String addressOriginating) {
        this.addressOriginating = addressOriginating;
    }

    public String getSbuOri() {
        return sbuOri;
    }

    public void setSbuOri(String sbuOri) {
        this.sbuOri = sbuOri;
    }

    public String getAddressTerminating() {
        return addressTerminating;
    }

    public void setAddressTerminating(String addressTerminating) {
        this.addressTerminating = addressTerminating;
    }

    public String getSbuTer() {
        return sbuTer;
    }

    public void setSbuTer(String sbuTer) {
        this.sbuTer = sbuTer;
    }

    public String getAgrId() {
        return agrId;
    }

    public void setAgrId(String agrId) {
        this.agrId = agrId;
    }

    public String getLayanan() {
        return layanan;
    }

    public void setLayanan(String layanan) {
        this.layanan = layanan;
    }

    public String getBandwidth() {
        return bandwidth;
    }

    public void setBandwidth(String bandwidth) {
        this.bandwidth = bandwidth;
    }

    public BigInteger getQty() {
        return qty;
    }

    public void setQty(BigInteger qty) {
        this.qty = qty;
    }

    public BigDecimal getBiayaSewa() {
        return biayaSewa;
    }

    public void setBiayaSewa(BigDecimal biayaSewa) {
        this.biayaSewa = biayaSewa;
    }

    public BigDecimal getBiayaInstalasi() {
        return biayaInstalasi;
    }

    public void setBiayaInstalasi(BigDecimal biayaInstalasi) {
        this.biayaInstalasi = biayaInstalasi;
    }

    public BigDecimal getBiayaRelokasi() {
        return biayaRelokasi;
    }

    public void setBiayaRelokasi(BigDecimal biayaRelokasi) {
        this.biayaRelokasi = biayaRelokasi;
    }

    public String getActivationactualdate() {
        return activationactualdate;
    }

    public void setActivationactualdate(String activationactualdate) {
        this.activationactualdate = activationactualdate;
    }

    public String getSbuOwner() {
        return sbuOwner;
    }

    public void setSbuOwner(String sbuOwner) {
        this.sbuOwner = sbuOwner;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getPaId() {
        return paId;
    }

    public void setPaId(String paId) {
        this.paId = paId;
    }

    public String getPaStatus() {
        return paStatus;
    }

    public void setPaStatus(String paStatus) {
        this.paStatus = paStatus;
    }

    public String getPsId() {
        return psId;
    }

    public void setPsId(String psId) {
        this.psId = psId;
    }

    public BigDecimal getPriceLama() {
        return priceLama;
    }

    public void setPriceLama(BigDecimal priceLama) {
        this.priceLama = priceLama;
    }

    public String getSidLama() {
        return sidLama;
    }

    public void setSidLama(String sidLama) {
        this.sidLama = sidLama;
    }

    public String getCreatedon() {
        return createdon;
    }

    public void setCreatedon(String createdon) {
        this.createdon = createdon;
    }

    public String getAlReferenceNo() {
        return alReferenceNo;
    }

    public void setAlReferenceNo(String alReferenceNo) {
        this.alReferenceNo = alReferenceNo;
    }

    public String getAlNo() {
        return alNo;
    }

    public void setAlNo(String alNo) {
        this.alNo = alNo;
    }

    public String getAlStatus() {
        return alStatus;
    }

    public void setAlStatus(String alStatus) {
        this.alStatus = alStatus;
    }

    public String getBaaId() {
        return baaId;
    }

    public void setBaaId(String baaId) {
        this.baaId = baaId;
    }

    public BigDecimal getHargaFinal() {
        return hargaFinal;
    }

    public void setHargaFinal(BigDecimal hargaFinal) {
        this.hargaFinal = hargaFinal;
    }

    public String getTahunBa() {
        return tahunBa;
    }

    public void setTahunBa(String tahunBa) {
        this.tahunBa = tahunBa;
    }

    public String getTargetSo() {
        return targetSo;
    }

    public void setTargetSo(String targetSo) {
        this.targetSo = targetSo;
    }

    public String getAdaAl() {
        return adaAl;
    }

    public void setAdaAl(String adaAl) {
        this.adaAl = adaAl;
    }

    public String getBa() {
        return ba;
    }

    public void setBa(String ba) {
        this.ba = ba;
    }

    public BigDecimal getPotensiBa() {
        return potensiBa;
    }

    public void setPotensiBa(BigDecimal potensiBa) {
        this.potensiBa = potensiBa;
    }

    public BigDecimal getOtc1() {
        return otc1;
    }

    public void setOtc1(BigDecimal otc1) {
        this.otc1 = otc1;
    }

    public BigDecimal getJumlahBa() {
        return jumlahBa;
    }

    public void setJumlahBa(BigDecimal jumlahBa) {
        this.jumlahBa = jumlahBa;
    }

    public BigDecimal getPotensiProgress() {
        return potensiProgress;
    }

    public void setPotensiProgress(BigDecimal potensiProgress) {
        this.potensiProgress = potensiProgress;
    }

    public BigDecimal getOtc2() {
        return otc2;
    }

    public void setOtc2(BigDecimal otc2) {
        this.otc2 = otc2;
    }

    public BigDecimal getJumlahSo() {
        return jumlahSo;
    }

    public void setJumlahSo(BigDecimal jumlahSo) {
        this.jumlahSo = jumlahSo;
    }

    public String getGroupOwner() {
        return groupOwner;
    }

    public void setGroupOwner(String groupOwner) {
        this.groupOwner = groupOwner;
    }

    public String getGroupLayanan() {
        return groupLayanan;
    }

    public void setGroupLayanan(String groupLayanan) {
        this.groupLayanan = groupLayanan;
    }

    public BigDecimal getBa2019() {
        return ba2019;
    }

    public void setBa2019(BigDecimal ba2019) {
        this.ba2019 = ba2019;
    }

    public BigDecimal getSo2019() {
        return so2019;
    }

    public void setSo2019(BigDecimal so2019) {
        this.so2019 = so2019;
    }

    public BigDecimal getCo2019() {
        return co2019;
    }

    public void setCo2019(BigDecimal co2019) {
        this.co2019 = co2019;
    }

    public BigDecimal getAging() {
        return aging;
    }

    public void setAging(BigDecimal aging) {
        this.aging = aging;
    }
    
}
