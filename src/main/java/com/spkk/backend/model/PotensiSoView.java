/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.spkk.backend.model;

import lombok.ToString;

import java.io.Serializable;
import java.math.BigInteger;
import javax.persistence.*;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author bayua
 */
@Entity
@Table(name = "POTENSI_SO")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "PotensiSoView.findAll", query = "SELECT p FROM PotensiSoView p")
    , @NamedQuery(name = "PotensiSoView.findByGroupOwner", query = "SELECT p FROM PotensiSoView p WHERE p.groupOwner = :groupOwner")
    , @NamedQuery(name = "PotensiSoView.findByPotensiSoOpen", query = "SELECT p FROM PotensiSoView p WHERE p.potensiSoOpen = :potensiSoOpen")
    , @NamedQuery(name = "PotensiSoView.findByPotensiSoClosed", query = "SELECT p FROM PotensiSoView p WHERE p.potensiSoClosed = :potensiSoClosed")
    , @NamedQuery(name = "PotensiSoView.findByPotensiSoProgress", query = "SELECT p FROM PotensiSoView p WHERE p.potensiSoProgress = :potensiSoProgress")})
@ToString(includeFieldNames=true)
public class PotensiSoView implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Size(max = 400)
    @Column(name = "GROUP_OWNER")
    private String groupOwner;
    @Column(name = "POTENSI_SO_OPEN")
    private BigInteger potensiSoOpen;
    @Column(name = "POTENSI_SO_CLOSED")
    private BigInteger potensiSoClosed;
    @Column(name = "POTENSI_SO_PROGRESS")
    private BigInteger potensiSoProgress;

    public PotensiSoView() {
    }

    public String getGroupOwner() {
        return groupOwner;
    }

    public void setGroupOwner(String groupOwner) {
        this.groupOwner = groupOwner;
    }

    public BigInteger getPotensiSoOpen() {
        return potensiSoOpen;
    }

    public void setPotensiSoOpen(BigInteger potensiSoOpen) {
        this.potensiSoOpen = potensiSoOpen;
    }

    public BigInteger getPotensiSoClosed() {
        return potensiSoClosed;
    }

    public void setPotensiSoClosed(BigInteger potensiSoClosed) {
        this.potensiSoClosed = potensiSoClosed;
    }

    public BigInteger getPotensiSoProgress() {
        return potensiSoProgress;
    }

    public void setPotensiSoProgress(BigInteger potensiSoProgress) {
        this.potensiSoProgress = potensiSoProgress;
    }
    
}
