package com.spkk.backend.config;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import java.sql.SQLException;
import java.util.Properties;

import javax.sql.DataSource;


import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.datatype.hibernate5.Hibernate5Module;
import oracle.jdbc.pool.OracleDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.session.jdbc.config.annotation.web.http.EnableJdbcHttpSession;
import org.springframework.transaction.annotation.EnableTransactionManagement;
/**
 *
 * @author Administrator
 */
@Configuration
@EnableTransactionManagement
@EnableJpaRepositories("com.spkk.backend.repository")
@PropertySource("classpath:application.properties")
@EnableJdbcHttpSession
public class DataConfig {


    private final String PROPERTY_URL = "spring.datasource.url";

    private final String PROPERTY_USERNAME = "spring.datasource.username";

    private final String PROPERTY_PASSWORD = "spring.datasource.password" ;

    private final String PROPERTY_DDL_AUTO = "spring.jpa.hibernate.ddl-auto";
    private final String PROPERTY_DIALECT = "spring.jpa.properties.hibernate.dialect";

    @Autowired
    Environment environment;

    @Bean
    LocalContainerEntityManagerFactoryBean entityManagerFactory() {

           Properties jpaProperties = new Properties();
           jpaProperties.put(PROPERTY_DIALECT, environment.getRequiredProperty(PROPERTY_DIALECT));
           jpaProperties.put(PROPERTY_DDL_AUTO, environment.getRequiredProperty(PROPERTY_DDL_AUTO));
           LocalContainerEntityManagerFactoryBean em = new  LocalContainerEntityManagerFactoryBean();
           em.setDataSource(dataSource());
           em.setJpaProperties(jpaProperties);
           em.setPackagesToScan("com.spkk.backend.model");
           JpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
           em.setJpaVendorAdapter(vendorAdapter);
           return em;
    }

    @Bean
    DataSource dataSource()
    {
    try{
        OracleDataSource dataSource = new OracleDataSource();
        dataSource.setUser(environment.getRequiredProperty(PROPERTY_USERNAME));
        dataSource.setPassword(environment.getRequiredProperty(PROPERTY_PASSWORD));
        dataSource.setURL(environment.getRequiredProperty(PROPERTY_URL));
        dataSource.setImplicitCachingEnabled(true);
        dataSource.setFastConnectionFailoverEnabled(true);
        return dataSource;}
        catch(Exception e){
        e.printStackTrace();
        }
        return null;
}


    @Bean
    JpaTransactionManager transactionManager() {
        JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(entityManagerFactory().getObject());
        return transactionManager;
    }

    @Bean
    public MappingJackson2HttpMessageConverter jackson2HttpMessageConverter() {
        MappingJackson2HttpMessageConverter converter = new MappingJackson2HttpMessageConverter();
        converter.setObjectMapper(this.jacksonBuilder().build());

        return converter;
    }

    public Jackson2ObjectMapperBuilder jacksonBuilder() {
        Jackson2ObjectMapperBuilder builder = new Jackson2ObjectMapperBuilder();

        Hibernate5Module hibernateModule = new Hibernate5Module();

        hibernateModule.configure(Hibernate5Module.Feature.FORCE_LAZY_LOADING, false);

        builder.modules(hibernateModule);

        // Spring MVC default Objectmapper configuration
        builder.featuresToDisable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
        builder.featuresToDisable(MapperFeature.DEFAULT_VIEW_INCLUSION);

        return builder;
    }


}

