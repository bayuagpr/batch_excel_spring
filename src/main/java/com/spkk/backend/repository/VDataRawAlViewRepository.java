package com.spkk.backend.repository;

import com.spkk.backend.model.VDataRawAlView;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.stereotype.Repository;

/**
* Generated by Spring Data Generator on 03/09/2018
*/
@Repository
public interface VDataRawAlViewRepository extends JpaRepository<VDataRawAlView, String>, JpaSpecificationExecutor<VDataRawAlView>, QuerydslPredicateExecutor<VDataRawAlView> {

}
