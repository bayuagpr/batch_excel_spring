package com.spkk.backend.batch.aluploadjob.listener;

import com.spkk.backend.model.ActivationListEntity;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.ItemProcessListener;

/**
 * @author Tobias Flohre
 */
@Slf4j
public class LogProcessListener implements ItemProcessListener<ActivationListEntity, ActivationListEntity> {

	//private static final Log log = LogFactory.getLog(LogProcessListener.class);
	
	public void afterProcess(ActivationListEntity item, ActivationListEntity result) {
		log.info("Input to Processor: "+item.toString());
		log.info("Output of Processor: "+result.toString());
	}

	public void beforeProcess(ActivationListEntity item) {
		log.info("Processing begin");
	}

	public void onProcessError(ActivationListEntity item, Exception e) {
	}

}
