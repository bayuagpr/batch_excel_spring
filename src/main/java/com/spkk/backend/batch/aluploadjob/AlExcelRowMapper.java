package com.spkk.backend.batch.aluploadjob;

import com.spkk.backend.model.ActivationListEntity;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.item.excel.RowMapper;
import org.springframework.batch.item.excel.support.rowset.RowSet;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

@Slf4j
public class AlExcelRowMapper implements RowMapper<ActivationListEntity> {

    @Override
    public ActivationListEntity mapRow(RowSet rowSet) throws Exception {
        ActivationListEntity al = new ActivationListEntity();

        al.setAlNo(rowSet.getColumnValue(0));
        al.setAlStatus(rowSet.getColumnValue(1));
        al.setAlType(rowSet.getColumnValue(2));
        al.setArNo(rowSet.getColumnValue(3));
        al.setArStatus(rowSet.getColumnValue(4));
        al.setPaId(rowSet.getColumnValue(5));
        al.setPaStatus(rowSet.getColumnValue(6));
        al.setCustid(rowSet.getColumnValue(7));
        al.setCustname(rowSet.getColumnValue(8));
        al.setSegment(rowSet.getColumnValue(9));
        al.setSid(rowSet.getColumnValue(10));
        al.setAddressOri(rowSet.getColumnValue(11));
        al.setSbuOri(rowSet.getColumnValue(12));
        al.setAddressTer(rowSet.getColumnValue(13));
        al.setSbuTer(rowSet.getColumnValue(14));
        al.setAgrNo(rowSet.getColumnValue(15));
        al.setLayanan(rowSet.getColumnValue(16));
        al.setBandwidth(rowSet.getColumnValue(17));
        NumberFormat nf = NumberFormat.getNumberInstance();
        DecimalFormat df = (DecimalFormat)nf;
        df.setParseBigDecimal(true);
        String strQty = rowSet.getColumnValue(18);
        String strBs = rowSet.getColumnValue(19);
        String strBi = rowSet.getColumnValue(20);
        String strBr = rowSet.getColumnValue(21);
        String strSl = rowSet.getColumnValue(22);
        BigDecimal decimal;
        Double dblSewa;
        Double dblInstalasi;
        Double dbRelokasi;
        Double dbSewaLama;
        if (strQty != null && strQty.length() > 0) {

            decimal = (BigDecimal)df.parse(strQty);

        }
        else{
            decimal = BigDecimal.valueOf(0.0);
        }
        if (strBs != null && strBs.length() > 0) {

            dblSewa = Double.parseDouble(strBs);

        }
        else{
            dblSewa = 0.0;
        }
        if (strBi != null && strBi.length() > 0) {

            dblInstalasi = Double.parseDouble(strBi);

        }
        else{
            dblInstalasi = 0.0;
        }
        if (strBr != null && strBr.length() > 0) {

            dbRelokasi = Double.parseDouble(strBr);

        }
        else{
            dbRelokasi = 0.0;
        }
        if (strSl != null && strSl.length() > 0) {

            dbSewaLama = Double.parseDouble(strSl);

        }
        else{
            dbSewaLama = 0.0;
        }
        BigInteger big = decimal.toBigInteger();
        al.setQty(big);
        al.setBiayaSewa(dblSewa);
        al.setBiayaInstalasi(dblInstalasi);
        al.setBiayaRelokasi(dbRelokasi);
        al.setBiayaSewaLama(dbSewaLama);
        al.setAlReference(rowSet.getColumnValue(23));
        if (strSl != null && strSl.length() > 0) {

            dbSewaLama = Double.parseDouble(strSl);

        }
        else{
            dbSewaLama = 0.0;
        }
        String sbd = rowSet.getColumnValue(24);
        String lid = rowSet.getColumnValue(25);
        String dd = rowSet.getColumnValue(26);
        String co = rowSet.getColumnValue(29);
        if (sbd != null && sbd.length() > 0) {
            Date parsedSbd = new Date(Long.parseLong(sbd));
            String strSbdBefore = new SimpleDateFormat().format(parsedSbd);
            SimpleDateFormat sdfSbd = new SimpleDateFormat();
            Date sbdFormat = sdfSbd.parse(strSbdBefore);
            sdfSbd.applyPattern("MM/dd/yyyy");
            String strSbd = sdfSbd.format(sbdFormat);
            al.setStartbilldate(new SimpleDateFormat("MM/dd/yyyy").parse(strSbd));
        }
        else{
            al.setStartbilldate(null);
        }

        if (lid != null && lid.length() > 0) {

            Date parsedLid = new Date(Long.parseLong(lid));
            String strLidBefore = new SimpleDateFormat().format(parsedLid);
            SimpleDateFormat sdfLid = new SimpleDateFormat();
            Date sbdFormat = sdfLid.parse(strLidBefore);
            sdfLid.applyPattern("MM/dd/yyyy");
            String strLid = sdfLid.format(sbdFormat);
            al.setLastinvoicedate(new SimpleDateFormat("MM/dd/yyyy").parse(strLid));

        }
        else{
            al.setLastinvoicedate(null);
        }

        if (dd != null && dd.length() > 0) {

            Date parsedDd = new Date(Long.parseLong(dd));
            String strDdBefore = new SimpleDateFormat().format(parsedDd);
            SimpleDateFormat sdfDd = new SimpleDateFormat();
            Date sbdFormat = sdfDd.parse(strDdBefore);
            sdfDd.applyPattern("MM/dd/yyyy");
            String strDd = sdfDd.format(sbdFormat);
            al.setDeactivationdate(new SimpleDateFormat("MM/dd/yyyy").parse(strDd));

        }
        else{
            al.setDeactivationdate(null);
        }

        if (co != null && co.length() > 0) {

            Date parsedCo = new Date(Long.parseLong(co));
            String strCoBefore  = new SimpleDateFormat().format(parsedCo);
            SimpleDateFormat sdfCo = new SimpleDateFormat();
            Date sbdFormat = sdfCo.parse(strCoBefore);
            sdfCo.applyPattern("MM/dd/yyyy");
            String strCo = sdfCo.format(sbdFormat);
            al.setCreatedon(new SimpleDateFormat("MM/dd/yyyy").parse(strCo));

        }
        else{
            al.setCreatedon(null);
        }

        al.setOwner(rowSet.getColumnValue(27));
        al.setSbuOwner(rowSet.getColumnValue(28));

        return al;
    }

}
