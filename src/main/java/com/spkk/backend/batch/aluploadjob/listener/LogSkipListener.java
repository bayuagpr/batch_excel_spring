package com.spkk.backend.batch.aluploadjob.listener;

import com.spkk.backend.model.ActivationListEntity;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.SkipListener;


/**
 * @author Tobias Flohre
 */
@Slf4j
public class LogSkipListener implements SkipListener<ActivationListEntity, ActivationListEntity> {

	//private static final Log LOGGER = LogFactory.getLog(LogSkipListener.class);

	public void onSkipInProcess(ActivationListEntity partner, Throwable throwable) {
		log.info("Row was skipped in process: "+partner+".",throwable);
	}

	public void onSkipInRead(Throwable arg0) {
	}

	public void onSkipInWrite(ActivationListEntity arg0, Throwable arg1) {
	}

}
