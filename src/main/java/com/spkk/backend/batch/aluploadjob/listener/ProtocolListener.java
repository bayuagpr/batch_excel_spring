package com.spkk.backend.batch.aluploadjob.listener;

import java.util.Iterator;
import java.util.Map.Entry;
import java.util.concurrent.TimeUnit;

import com.spkk.backend.repository.ActivationListEntityRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobExecutionListener;
import org.springframework.batch.core.JobParameter;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.StepExecution;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author Tobias Flohre
 */
@Slf4j
public class ProtocolListener implements JobExecutionListener {

	//private static final Log LOGGER = LogFactory.getLog(ProtocolListener.class);

    @Autowired
    ActivationListEntityRepository alDao;

	public void afterJob(JobExecution jobExecution) {
		StringBuilder protocol = new StringBuilder();
		protocol.append("\n+++++++++++++++++++++++++++++++++++++++++++++++++++++++ \n");
		protocol.append("Protocol for " + jobExecution.getJobInstance().getJobName() + " \n");
		protocol.append("  Started     : "+ jobExecution.getStartTime()+"\n");
		protocol.append("  Finished    : "+ jobExecution.getEndTime()+"\n");
		long diffInMillies = Math.abs(jobExecution.getEndTime().getTime() - jobExecution.getStartTime().getTime());
		long diff = TimeUnit.MINUTES.convert(diffInMillies, TimeUnit.MILLISECONDS);
		protocol.append("  Duration    : "+ diff+"\n");
        protocol.append("Row Counter: "+Long.toString(alDao.count())+"\n");
		protocol.append("  Exit-Code   : "+ jobExecution.getExitStatus().getExitCode()+"\n");
		protocol.append("  Exit-Descr. : "+ jobExecution.getExitStatus().getExitDescription()+"\n");
		protocol.append("  Status      : "+ jobExecution.getStatus()+"\n");
		protocol.append("+++++++++++++++++++++++++++++++++++++++++++++++++++++++ \n");

		protocol.append("Job-Parameter: \n");		
		JobParameters jp = jobExecution.getJobParameters();
		for (Iterator<Entry<String,JobParameter>> iter = jp.getParameters().entrySet().iterator(); iter.hasNext();) {
			Entry<String,JobParameter> entry = iter.next();
			protocol.append("  "+entry.getKey()+"="+entry.getValue()+"\n");
		}
		protocol.append("+++++++++++++++++++++++++++++++++++++++++++++++++++++++ \n");		
		
		for (StepExecution stepExecution : jobExecution.getStepExecutions()) {
			protocol.append("\n+++++++++++++++++++++++++++++++++++++++++++++++++++++++ \n");
			protocol.append("Step " + stepExecution.getStepName() + " \n");
			protocol.append("ReadCount: "+ stepExecution.getReadCount()+ "\n");
			protocol.append("WriteCount: " + stepExecution.getWriteCount() + "\n");
			protocol.append("Commits: " + stepExecution.getCommitCount() + "\n");
			protocol.append("SkipCount: " + stepExecution.getSkipCount() + "\n");
			protocol.append("Rollbacks: " + stepExecution.getRollbackCount() + "\n");
			protocol.append("Filter: " + stepExecution.getFilterCount() + "\n");					
			protocol.append("+++++++++++++++++++++++++++++++++++++++++++++++++++++++ \n");
		}
		log.info(protocol.toString());
	}

	public void beforeJob(JobExecution arg0) {
		// nothing to do
	}

}
