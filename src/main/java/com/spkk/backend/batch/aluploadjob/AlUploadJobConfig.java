package com.spkk.backend.batch.aluploadjob;

import com.spkk.backend.batch.aluploadjob.listener.*;
import com.spkk.backend.batch.utils.ExcelItemReader;
import com.spkk.backend.model.ActivationListEntity;
import com.spkk.backend.repository.ActivationListEntityRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.data.RepositoryItemWriter;
import org.springframework.batch.item.excel.ExcelFileParseException;
import org.springframework.batch.item.excel.RowMapper;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.core.io.ByteArrayResource;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.PushbackInputStream;
import java.util.Arrays;

@Configuration
@PropertySource("classpath:application.properties")
@Slf4j
public class AlUploadJobConfig {
    private static final String PROPERTY_EXCEL_SOURCE_FILE_PATH = "excel.to.database.job.source.file.path.al";

    @Bean
    ItemReader<ActivationListEntity> excelAlReader(Environment environment) throws IOException {
        FileInputStream fileInputStream = new FileInputStream(environment.getRequiredProperty(PROPERTY_EXCEL_SOURCE_FILE_PATH));
        PushbackInputStream input = new PushbackInputStream(fileInputStream);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        org.apache.commons.io.IOUtils.copy(input, baos);
        byte[] bytes = baos.toByteArray();
        ExcelItemReader<ActivationListEntity> reader = new ExcelItemReader<>();
        reader.setLinesToSkip(1);
        reader.setResource(new ByteArrayResource(bytes));
        reader.setRowMapper(excelAlRowMapper());
        reader.setSkippedRowsCallback(rs -> log.info("Skipping: " + Arrays.toString(rs.getCurrentRow())));

        return reader;
    }


    private RowMapper<ActivationListEntity> excelAlRowMapper(){
        return new AlExcelRowMapper();
    }

    @Bean
    ItemProcessor<ActivationListEntity, ActivationListEntity> excelAlProcessor() {
        return new ValidationAlProcessor();
    }


    @Bean
    public RepositoryItemWriter<ActivationListEntity> excelAlWriter(final ActivationListEntityRepository alDao) {
        RepositoryItemWriter<ActivationListEntity> writer = new RepositoryItemWriter<>();
        writer.setRepository(alDao);
        writer.setMethodName("save");
        return writer;
    }

    @Bean
    Step excelAlFileToDatabaseStep(ItemReader<ActivationListEntity> excelAlReader,
                                 ItemProcessor<ActivationListEntity, ActivationListEntity> excelAlProcessor,
                                 ItemWriter<ActivationListEntity> excelAlWriter,
                                 StepBuilderFactory stepBuilderFactory) {
        return stepBuilderFactory.get("excelAlFileToDatabaseStep")
                .<ActivationListEntity, ActivationListEntity>chunk(100)
                .reader(excelAlReader).listener(logReadAlListener())
                .processor(excelAlProcessor).listener(logProcessAlListener())
                .writer(excelAlWriter).listener(logWriteAlListener())
                .faultTolerant()
                .skipLimit(10)
                .skip(ExcelFileParseException.class)
                .listener(logSkipAlListener())
                .build();
    }

    @Bean
    Job excelAlFileToDatabaseJob(JobBuilderFactory jobBuilderFactory,
                               @Qualifier("excelAlFileToDatabaseStep") Step excelAlStep) {
        return jobBuilderFactory.get("excelAlFileToDatabaseJob").listener(protocolAlListener())
                .incrementer(new RunIdIncrementer())
                .flow(excelAlStep)
                .end()
                .build();
    }

    @Bean
    public ProtocolListener protocolAlListener(){
        return new ProtocolListener();
    }

    @Bean
    public LogProcessListener logProcessAlListener(){
        return new LogProcessListener();
    }

    @Bean
    public LogSkipListener logSkipAlListener(){
        return new LogSkipListener();
    }

    @Bean
    public LogReadListener logReadAlListener(){return new LogReadListener();}

    @Bean
    public LogWriteListener logWriteAlListener(){return new LogWriteListener();}
}
