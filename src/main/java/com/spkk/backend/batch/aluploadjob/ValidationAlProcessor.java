package com.spkk.backend.batch.aluploadjob;

import com.spkk.backend.model.ActivationListEntity;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemProcessor;

/**
 * This custom {@code ItemProcessor} simply writes the information of the
 * processed student to the log and returns the processed object.
 *
 * @author Petri Kainulainen
 */
@Slf4j
public class ValidationAlProcessor implements ItemProcessor<ActivationListEntity, ActivationListEntity> {


    @Override
    public ActivationListEntity process(ActivationListEntity item) throws Exception {
        if(item.getAlNo().isEmpty()){
            throw new RuntimeException("Al Number must be added!");
        }
        return item;
    }
}
