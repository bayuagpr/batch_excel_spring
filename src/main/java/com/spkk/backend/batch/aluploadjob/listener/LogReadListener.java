package com.spkk.backend.batch.aluploadjob.listener;


import org.springframework.batch.core.ItemReadListener;
import com.spkk.backend.model.ActivationListEntity;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class LogReadListener implements ItemReadListener<ActivationListEntity> {

    public void afterRead(ActivationListEntity item){
        log.info("Data to be process: " +item.toString());

    }

    public void beforeRead() {
        log.info("Start read data");
    }

    public void onReadError(Exception e) {
    }
}
