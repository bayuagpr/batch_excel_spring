package com.spkk.backend.batch.aruploadjob.listener;

import com.spkk.backend.model.ActivationRequestEntity;
import com.spkk.backend.repository.ActivationListEntityRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.ItemWriteListener;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

@Slf4j
public class LogWriteListener implements ItemWriteListener<ActivationRequestEntity> {
    @Autowired
    ActivationListEntityRepository alDao;

    public void afterWrite(List<? extends ActivationRequestEntity> items){
        //log.info("Row Counter: "+Long.toString(alDao.count()));
        //items.forEach(i -> log.debug("State of a Activation List upload succeed: {}", i.equals(alDao.getOne(i.getAlNo()))));

    }

    public void beforeWrite(List<? extends ActivationRequestEntity> items) {
        log.info("Start Write data");
        log.info("Received the information of {} students", items.size());


    }

    public void onWriteError(Exception e, List<? extends ActivationRequestEntity> items) {
    }
}
