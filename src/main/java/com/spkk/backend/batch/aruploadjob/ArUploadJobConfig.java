package com.spkk.backend.batch.aruploadjob;

import com.spkk.backend.batch.aruploadjob.listener.*;
import com.spkk.backend.batch.utils.ExcelItemReader;
import com.spkk.backend.model.ActivationRequestEntity;
import com.spkk.backend.repository.ActivationRequestEntityRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.data.RepositoryItemWriter;
import org.springframework.batch.item.excel.ExcelFileParseException;
import org.springframework.batch.item.excel.RowMapper;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.core.io.ByteArrayResource;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.PushbackInputStream;
import java.util.Arrays;

@Configuration
@PropertySource("classpath:application.properties")
@Slf4j
public class ArUploadJobConfig {
    private static final String PROPERTY_EXCEL_SOURCE_FILE_PATH = "excel.to.database.job.source.file.path.ar";

    @Bean
    ItemReader<ActivationRequestEntity> excelArReader(Environment environment) throws IOException {
        FileInputStream fileInputStream = new FileInputStream(environment.getRequiredProperty(PROPERTY_EXCEL_SOURCE_FILE_PATH));
        PushbackInputStream input = new PushbackInputStream(fileInputStream);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        org.apache.commons.io.IOUtils.copy(input, baos);
        byte[] bytes = baos.toByteArray();
        ExcelItemReader<ActivationRequestEntity> reader = new ExcelItemReader<>();
        reader.setLinesToSkip(1);
        reader.setResource(new ByteArrayResource(bytes));
        reader.setRowMapper(excelArRowMapper());
        reader.setSkippedRowsCallback(rs -> log.info("Skipping: " + Arrays.toString(rs.getCurrentRow())));

        return reader;
    }

    private RowMapper<ActivationRequestEntity> excelArRowMapper(){
        return new ArExcelRowMapper();
    }

    @Bean
    ItemProcessor<ActivationRequestEntity, ActivationRequestEntity> excelArProcessor() {
        return new ValidationArProcessor();
    }


    @Bean
    public RepositoryItemWriter<ActivationRequestEntity> excelArWriter(final ActivationRequestEntityRepository arDao) {
        RepositoryItemWriter<ActivationRequestEntity> writer = new RepositoryItemWriter<>();
        writer.setRepository(arDao);
        writer.setMethodName("save");
        return writer;
    }

    @Bean
    Step excelArFileToDatabaseStep(ItemReader<ActivationRequestEntity> excelArReader,
                                 ItemProcessor<ActivationRequestEntity, ActivationRequestEntity> excelArProcessor,
                                 ItemWriter<ActivationRequestEntity> excelArWriter,
                                 StepBuilderFactory stepBuilderFactory) {
        return stepBuilderFactory.get("excelArFileToDatabaseStep")
                .<ActivationRequestEntity, ActivationRequestEntity>chunk(100)
                .reader(excelArReader).listener(logReadArListener())
                .processor(excelArProcessor).listener(logProcessArListener())
                .writer(excelArWriter).listener(logWriteArListener())
                .faultTolerant()
                .skipLimit(10)
                .skip(ExcelFileParseException.class)
                .listener(logSkipArListener())
                .build();
    }

    @Bean
    Job excelArFileToDatabaseJob(JobBuilderFactory jobBuilderFactory,
                               @Qualifier("excelArFileToDatabaseStep") Step excelArStep) {
        return jobBuilderFactory.get("excelArFileToDatabaseJob").listener(protocolArListener())
                .incrementer(new RunIdIncrementer())
                .flow(excelArStep)
                .end()
                .build();
    }

    @Bean
    public ProtocolListener protocolArListener(){
        return new ProtocolListener();
    }

    @Bean
    public LogProcessListener logProcessArListener(){
        return new LogProcessListener();
    }

    @Bean
    public LogSkipListener logSkipArListener(){
        return new LogSkipListener();
    }

    @Bean
    public LogReadListener logReadArListener(){return new LogReadListener();}

    @Bean
    public LogWriteListener logWriteArListener(){return new LogWriteListener();}
}
