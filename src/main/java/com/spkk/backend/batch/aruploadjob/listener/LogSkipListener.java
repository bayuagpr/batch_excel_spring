package com.spkk.backend.batch.aruploadjob.listener;

import com.spkk.backend.model.ActivationRequestEntity;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.SkipListener;


/**
 * @author Tobias Flohre
 */
@Slf4j
public class LogSkipListener implements SkipListener<ActivationRequestEntity, ActivationRequestEntity> {

	//private static final Log LOGGER = LogFactory.getLog(LogSkipListener.class);

	public void onSkipInProcess(ActivationRequestEntity partner, Throwable throwable) {
		log.info("Row was skipped in process: "+partner+".",throwable);
	}

	public void onSkipInRead(Throwable arg0) {
	}

	public void onSkipInWrite(ActivationRequestEntity arg0, Throwable arg1) {
	}

}
