package com.spkk.backend.batch.aruploadjob.listener;

import com.spkk.backend.model.ActivationRequestEntity;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.ItemProcessListener;

/**
 * @author Tobias Flohre
 */
@Slf4j
public class LogProcessListener implements ItemProcessListener<ActivationRequestEntity, ActivationRequestEntity> {

	//private static final Log log = LogFactory.getLog(LogProcessListener.class);
	
	public void afterProcess(ActivationRequestEntity item, ActivationRequestEntity result) {
		log.info("Input to Processor: "+item.toString());
		log.info("Output of Processor: "+result.toString());
	}

	public void beforeProcess(ActivationRequestEntity item) {
		log.info("Processing begin");
	}

	public void onProcessError(ActivationRequestEntity item, Exception e) {
	}

}
