 package com.spkk.backend.batch.aruploadjob;

import com.spkk.backend.model.ActivationRequestEntity;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.item.excel.RowMapper;
import org.springframework.batch.item.excel.support.rowset.RowSet;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

@Slf4j
public class ArExcelRowMapper implements RowMapper<ActivationRequestEntity> {

    @Override
    public ActivationRequestEntity mapRow(RowSet rowSet) throws Exception {
        ActivationRequestEntity ar = new ActivationRequestEntity();

        ar.setArNo(rowSet.getColumnValue(0));
        ar.setArType(rowSet.getColumnValue(1));
        ar.setCustName(rowSet.getColumnValue(2));
        ar.setSegment(rowSet.getColumnValue(3));
        ar.setArStatus(rowSet.getColumnValue(4));
        String ad = rowSet.getColumnValue(5);
        if (ad != null && ad.length() > 0) {
            Date parsedAd = new Date(Long.parseLong(ad));
            String strAdBefore = new SimpleDateFormat().format(parsedAd);
            SimpleDateFormat sdfAd = new SimpleDateFormat();
            Date adFormat = sdfAd.parse(strAdBefore);
            sdfAd.applyPattern("MM/dd/yyyy");
            String strAd = sdfAd.format(adFormat);
            ar.setArDate(new SimpleDateFormat("MM/dd/yyyy").parse(strAd));
        }
        else{
            ar.setArDate(null);
        }
        String bd = rowSet.getColumnValue(6);
        if (bd != null && bd.length() > 0) {
            Date parsedBd = new Date(Long.parseLong(bd));
            String strBdBefore = new SimpleDateFormat().format(parsedBd);
            SimpleDateFormat sdfBd = new SimpleDateFormat();
            Date bdFormat = sdfBd.parse(strBdBefore);
            sdfBd.applyPattern("MM/dd/yyyy");
            String strBd = sdfBd.format(bdFormat);
            ar.setBaDate(new SimpleDateFormat("MM/dd/yyyy").parse(strBd));
        }
        else{
            ar.setBaDate(null);
        }
        String cd = rowSet.getColumnValue(7);
        if (cd != null && cd.length() > 0) {
            Date parsedCd = new Date(Long.parseLong(cd));
            String strCdBefore = new SimpleDateFormat().format(parsedCd);
            SimpleDateFormat sdfCd = new SimpleDateFormat();
            Date cdFormat = sdfCd.parse(strCdBefore);
            sdfCd.applyPattern("MM/dd/yyyy");
            String strCd = sdfCd.format(cdFormat);
            ar.setCancelDate(new SimpleDateFormat("MM/dd/yyyy").parse(strCd));
        }
        else{
            ar.setCancelDate(null);
        }
        String apd = rowSet.getColumnValue(8);
        if (apd != null && apd.length() > 0) {
            Date parsedApd = new Date(Long.parseLong(apd));
            String strApdBefore = new SimpleDateFormat().format(parsedApd);
            SimpleDateFormat sdfApd = new SimpleDateFormat();
            Date apdFormat = sdfApd.parse(strApdBefore);
            sdfApd.applyPattern("MM/dd/yyyy");
            String strApd = sdfApd.format(apdFormat);
            ar.setApprovalDate(new SimpleDateFormat("MM/dd/yyyy").parse(strApd));
        }
        else{
            ar.setApprovalDate(null);
        }
        String td = rowSet.getColumnValue(9);
        if (td != null && td.length() > 0) {
            Date parsedTd = new Date(Long.parseLong(td));
            String strTdBefore = new SimpleDateFormat().format(parsedTd);
            SimpleDateFormat sdfTd = new SimpleDateFormat();
            Date tdFormat = sdfTd.parse(strTdBefore);
            sdfTd.applyPattern("MM/dd/yyyy");
            String strTd = sdfTd.format(tdFormat);
            ar.setTargetdate(new SimpleDateFormat("MM/dd/yyyy").parse(strTd));
        }
        else{
            ar.setTargetdate(null);
        }
        String cld = rowSet.getColumnValue(10);
        if (cld != null && cld.length() > 0) {
            Date parsedCld = new Date(Long.parseLong(cld));
            String strCldBefore = new SimpleDateFormat().format(parsedCld);
            SimpleDateFormat sdfCld = new SimpleDateFormat();
            Date cldFormat = sdfCld.parse(strCldBefore);
            sdfCld.applyPattern("MM/dd/yyyy");
            String strCld = sdfCld.format(cldFormat);
            ar.setClosedate(new SimpleDateFormat("MM/dd/yyyy").parse(strCld));
        }
        else{
            ar.setClosedate(null);
        }
        String aad = rowSet.getColumnValue(11);
        if (aad != null && aad.length() > 0) {
            Date parsedAad = new Date(Long.parseLong(aad));
            String strAadBefore = new SimpleDateFormat().format(parsedAad);
            SimpleDateFormat sdfAad = new SimpleDateFormat();
            Date aadFormat = sdfAad.parse(strAadBefore);
            sdfAad.applyPattern("MM/dd/yyyy");
            String strAad = sdfAad.format(aadFormat);
            ar.setActivationactualdate(new SimpleDateFormat("MM/dd/yyyy").parse(strAad));
        }
        else{
            ar.setActivationactualdate(null);
        }
        String dd = rowSet.getColumnValue(12);
        if (dd != null && dd.length() > 0) {
            Date parsedDd = new Date(Long.parseLong(dd));
            String strDdBefore = new SimpleDateFormat().format(parsedDd);
            SimpleDateFormat sdfDd = new SimpleDateFormat();
            Date ddFormat = sdfDd.parse(strDdBefore);
            sdfDd.applyPattern("MM/dd/yyyy");
            String strDd = sdfDd.format(ddFormat);
            ar.setDeactivationdate(new SimpleDateFormat("MM/dd/yyyy").parse(strDd));
        }
        else{
            ar.setDeactivationdate(null);
        }
        ar.setSid(rowSet.getColumnValue(13));
        ar.setLayanan(rowSet.getColumnValue(14));
        NumberFormat nf = NumberFormat.getNumberInstance();
        DecimalFormat df = (DecimalFormat)nf;
        df.setParseBigDecimal(true);
        String strQty = rowSet.getColumnValue(15);
        ar.setBandwidth(rowSet.getColumnValue(16));
        String strBs = rowSet.getColumnValue(17);
        String strBi = rowSet.getColumnValue(18);
        String strBr = rowSet.getColumnValue(19);
        BigDecimal decimal;
        Double dblSewa;
        Double dblInstalasi;
        Double dbRelokasi;
        if (strQty != null && strQty.length() > 0) {

            decimal = (BigDecimal)df.parse(strQty);

        }
        else{
            decimal = BigDecimal.valueOf(0.0);
        }
        if (strBs != null && strBs.length() > 0) {

            dblSewa = Double.parseDouble(strBs);

        }
        else{
            dblSewa = 0.0;
        }
        if (strBi != null && strBi.length() > 0) {

            dblInstalasi = Double.parseDouble(strBi);

        }
        else{
            dblInstalasi = 0.0;
        }
        if (strBr != null && strBr.length() > 0) {

            dbRelokasi = Double.parseDouble(strBr);

        }
        else{
            dbRelokasi = 0.0;
        }

        BigInteger big = decimal.toBigInteger();
        ar.setQty(big);
        ar.setBiayaSewa(dblSewa);
        ar.setBiayaInstalasi(dblInstalasi);
        ar.setBiayaRelokasi(dbRelokasi);
        ar.setAddressOriginating(rowSet.getColumnValue(20));
        ar.setSbuOri(rowSet.getColumnValue(21));
        ar.setAddressTerminating(rowSet.getColumnValue(22));
        ar.setSbuTer(rowSet.getColumnValue(23));
        ar.setOwner(rowSet.getColumnValue(24));
        ar.setSbuOwner(rowSet.getColumnValue(25));
        ar.setAgrId(rowSet.getColumnValue(26));
        ar.setPaId(rowSet.getColumnValue(27));
        ar.setPaStatus(rowSet.getColumnValue(28));
        ar.setPsId(rowSet.getColumnValue(29));
        ar.setBaaId(rowSet.getColumnValue(30));
        ar.setAlNo(rowSet.getColumnValue(31));
        ar.setAlStatus(rowSet.getColumnValue(32));
        ar.setAlReferenceNo(rowSet.getColumnValue(33));
        String strPl = rowSet.getColumnValue(34);
        Double dbPl;
        if (strPl != null && strPl.length() > 0) {

            dbPl = Double.parseDouble(strPl);

        }
        else{
            dbPl = 0.0;
        }
        ar.setPriceLama(dbPl);
        ar.setSidLama(rowSet.getColumnValue(35));
        String co = rowSet.getColumnValue(36);
        if (co != null && co.length() > 0) {

            Date parsedCo = new Date(Long.parseLong(co));
            String strCoBefore  = new SimpleDateFormat().format(parsedCo);
            SimpleDateFormat sdfCo = new SimpleDateFormat();
            Date sbdFormat = sdfCo.parse(strCoBefore);
            sdfCo.applyPattern("MM/dd/yyyy");
            String strCo = sdfCo.format(sbdFormat);
            ar.setCreatedon(new SimpleDateFormat("MM/dd/yyyy").parse(strCo));

        }
        else{
            ar.setCreatedon(null);
        }

        return ar;
    }

}
