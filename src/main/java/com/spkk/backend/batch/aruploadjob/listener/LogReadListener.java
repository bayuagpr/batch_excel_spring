package com.spkk.backend.batch.aruploadjob.listener;


import com.spkk.backend.model.ActivationRequestEntity;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.ItemReadListener;

@Slf4j
public class LogReadListener implements ItemReadListener<ActivationRequestEntity> {

    public void afterRead(ActivationRequestEntity item){
        log.info("Data to be process: " +item.toString());

    }

    public void beforeRead() {
        log.info("Start read data");
    }

    public void onReadError(Exception e) {
    }
}
