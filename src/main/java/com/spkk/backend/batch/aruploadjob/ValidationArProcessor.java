package com.spkk.backend.batch.aruploadjob;

import com.spkk.backend.model.ActivationRequestEntity;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.item.ItemProcessor;

/**
 * This custom {@code ItemProcessor} simply writes the information of the
 * processed student to the log and returns the processed object.
 *
 * @author Petri Kainulainen
 */
@Slf4j
public class ValidationArProcessor implements ItemProcessor<ActivationRequestEntity, ActivationRequestEntity> {


    @Override
    public ActivationRequestEntity process(ActivationRequestEntity item) throws Exception {
        if(item.getArNo().isEmpty()){
            throw new RuntimeException("Ar Number must be added!");
        }
        return item;
    }
}
