/*
 * Copyright 2006-2014 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.spkk.backend.batch.utils;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.usermodel.FormulaEvaluator;
import org.apache.poi.ss.usermodel.Row;
import org.springframework.batch.item.excel.Sheet;

public class PoiSheet implements Sheet {
    private final org.apache.poi.ss.usermodel.Sheet delegate;
    private final int numberOfRows;
    private final String name;
    private int numberOfColumns = -1;
    private FormulaEvaluator evaluator;

    PoiSheet(org.apache.poi.ss.usermodel.Sheet delegate) {
        this.delegate = delegate;
        this.numberOfRows = this.delegate.getLastRowNum() + 1;
        this.name = this.delegate.getSheetName();
    }

    public int getNumberOfRows() {
        return this.numberOfRows;
    }

    public String getName() {
        return this.name;
    }

    public String[] getRow(int rowNumber) {
        Row row = this.delegate.getRow(rowNumber);
        if (row == null) {
            return null;
        } else {
            List<String> cells = new LinkedList();

            for(int i = 0; i < this.getNumberOfColumns(); ++i) {
                Cell cell = row.getCell(i);
                switch(cell.getCellType()) {
                    case 0:
                        if (DateUtil.isCellDateFormatted(cell)) {
                            Date date = cell.getDateCellValue();
                            cells.add(String.valueOf(date.getTime()));
                        } else {
                            cells.add(String.valueOf(cell.getNumericCellValue()));
                        }
                        break;
                    case 1:
                    case 3:
                        cells.add(cell.getStringCellValue());
                        break;
                    case 2:
                        cells.add(this.getFormulaEvaluator().evaluate(cell).formatAsString());
                        break;
                    case 4:
                        cells.add(String.valueOf(cell.getBooleanCellValue()));
                        break;
                    default:
                        throw new IllegalArgumentException("Cannot handle cells of type " + cell.getCellType());
                }
            }

            return (String[])cells.toArray(new String[cells.size()]);
        }
    }

    private FormulaEvaluator getFormulaEvaluator() {
        if (this.evaluator == null) {
            this.evaluator = this.delegate.getWorkbook().getCreationHelper().createFormulaEvaluator();
        }

        return this.evaluator;
    }

    public int getNumberOfColumns() {
        if (this.numberOfColumns < 0) {
            this.numberOfColumns = this.delegate.getRow(0).getLastCellNum();
        }

        return this.numberOfColumns;
    }
}
