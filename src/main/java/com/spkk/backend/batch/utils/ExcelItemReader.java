package com.spkk.backend.batch.utils;

import lombok.extern.slf4j.Slf4j;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.springframework.batch.item.excel.Sheet;
import org.springframework.batch.item.excel.poi.PoiItemReader;
import org.springframework.core.io.Resource;

import java.io.Closeable;
import java.io.InputStream;
import java.io.PushbackInputStream;

@Slf4j
public class ExcelItemReader<T> extends PoiItemReader<T> {
    private Workbook workbook;
    private InputStream workbookStream;

    public ExcelItemReader() {
    }

    @Override
    protected Sheet getSheet(int sheet) {
        int sheetExact = 0;
        return new PoiSheet(this.workbook.getSheetAt(sheetExact));
    }

    @Override
    protected int getNumberOfSheets() {
        return this.workbook.getNumberOfSheets();
    }

    @Override
    protected void doClose() throws Exception {
        if (this.workbook instanceof Closeable) {
            this.workbook.close();
        }

        if (this.workbookStream != null) {
            this.workbookStream.close();
        }

        this.workbook = null;
        this.workbookStream = null;
    }

    @Override
    protected void openExcelFile(Resource resource) throws Exception {
        this.workbookStream = resource.getInputStream();
        if (!this.workbookStream.markSupported() && !(this.workbookStream instanceof PushbackInputStream)) {
            throw new IllegalStateException("InputStream MUST either support mark/reset, or be wrapped as a PushbackInputStream");
        } else {
            this.workbook = WorkbookFactory.create(this.workbookStream);
            this.workbook.setMissingCellPolicy(Row.CREATE_NULL_AS_BLANK);
        }
    }
}
